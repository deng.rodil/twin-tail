﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

using TwinTail.Entities;

namespace TwinTail.Api
{
    public class HummingbirdApiClient : ApiClient
    {
        private static readonly string API_KEY = "3jvKLDmpdVAtDzaTFtkGZ9fTVklKNE7o"; // This is a test key (replace it with production key)
        private static readonly string URL_AUTHENTICATION = "https://hummingbirdv1.p.mashape.com/users/authenticate";
        private static readonly string URL_ANIME_LIST = "https://hummingbirdv1.p.mashape.com/users/";
        //private static readonly string URL_ANIME_SEARCH = "http://api.herro.co/search/anime/";
        private static readonly string URL_ANIME_GET = "https://hummingbirdv1.p.mashape.com/anime/";
        private static readonly string URL_ANIME_UPDATE = "https://hummingbirdv1.p.mashape.com/libraries/";

        private string authToken = null;

        public override string Id
        {
            get
            {
                return "Hummingbird";
            }
        }

        #region Authentication
        public override async Task Authenticate(string user, string password)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("X-Mashape-Authorization", API_KEY);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("username", user.ToLower()),
                new KeyValuePair<string, string>("password", password)
            });

            // Run HTTP request
            HttpResponseMessage response = await httpClient.PostAsync(URL_AUTHENTICATION, content);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized) throw new AuthenticationFailureException(this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Authenticate
            string responseString = await response.Content.ReadAsStringAsync();
            authToken =  responseString.Replace("\"", "");
            User = user;
            Authenticated = true;
        }
        #endregion

        #region Read API
        public override async Task<IEnumerable<WatchEntry>> GetWatchEntries(string user)
        {
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Run HTTP Request
            string url = string.Format("{0}{1}/library?auth_token={2}", URL_ANIME_LIST, user.ToLower(), authToken);
            url = Uri.EscapeUriString(url);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for errors
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new UserNotFoundException(user, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Read JSON
            string responseString = await response.Content.ReadAsStringAsync();
            JArray values = JArray.Parse(responseString);

            // Parse each json anime objects
            List<WatchEntry> list = new List<WatchEntry>();
            foreach (JToken entry in values)
            {
                JToken animeToken = entry["anime"];
                Anime anime = new Anime();
                anime.Api = Id;
                anime.ServerId = (string)animeToken["slug"];
                anime.Slug = (string)animeToken["slug"];
                anime.Title = (string)animeToken["title"];
                anime.AirStatus = GetAirStatus((string)animeToken["status"]);
                anime.Type = GetAnimeType((string)animeToken["show_type"]);
                anime.Episodes = (int)animeToken["episode_count"];
                anime.ImageUrl = (string)animeToken["cover_image"];
                anime.Synopsis = (string)animeToken["synopsis"];

                JToken synonym = animeToken["alternate_title"];
                if (synonym.Type != JTokenType.Null)
                {
                    string val = (string)synonym;
                    if (!string.IsNullOrEmpty(val)) anime.Synonyms.Add(new Synonym(val));
                }

                WatchEntry watchEntry = new WatchEntry();
                watchEntry.Anime = anime;
                watchEntry.User = User;
                watchEntry.Episode = (int)entry["episodes_watched"];
                JToken dateUpdated = entry["last_watched"];
                if (dateUpdated.Type != JTokenType.Null) watchEntry.DateUpdated = (DateTime)dateUpdated;
                watchEntry.Notes = (string)entry["notes"];
                watchEntry.Status = GetWatchStatus((string)entry["status"]);

                JToken score = entry["rating"]["value"];
                if (score.HasValues) watchEntry.Score = (decimal)score * 2;

                list.Add(watchEntry);
                if (AnimeFetched != null) AnimeFetched(anime, this);
            }

            return new ReadOnlyCollection<WatchEntry>(list);
        }

        public override Task<IEnumerable<Anime>> SearchAnime(string query)
        {
            throw new ApiMethodNotSupported(this);
        }

        public override async Task<Anime> GetAnime(string animeId)
        {
            string url = URL_ANIME_GET + animeId;
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for errors
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new AnimeNotFoundException(animeId, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Get json
            string responseString = await response.Content.ReadAsStringAsync();
            JObject animeToken = JObject.Parse(responseString);

            Anime anime = new Anime();
            anime.Api = Id;
            anime.ServerId = (string)animeToken["slug"];
            anime.Slug = (string)animeToken["slug"];
            anime.Title = (string)animeToken["title"];
            anime.AirStatus = GetAirStatus((string)animeToken["status"]);
            anime.Type = GetAnimeType((string)animeToken["show_type"]);
            anime.Episodes = (int)animeToken["episode_count"];
            anime.ImageUrl = (string)animeToken["cover_image"];
            anime.Synopsis = (string)animeToken["synopsis"];

            JToken synonym = animeToken["alternate_title"];
            if (synonym.Type != JTokenType.Null)
            {
                string val = (string)synonym;
                if (!string.IsNullOrEmpty(val)) anime.Synonyms.Add(new Synonym(val));
            }

            // Genre
            foreach (JToken genre in animeToken["genres"])
            {
                string val = (string)genre;
                if (!string.IsNullOrEmpty(val)) anime.Genres.Add(new Genre(val));
            }

            if (AnimeFetched != null) AnimeFetched(anime, this);
            return anime;
        }

        public override async Task<IEnumerable<Anime>> GetDatabase()
        {
            await Task.Delay(0);
            throw new ApiMethodNotSupported(this);
        }

        public override async Task<Anime> Match(Anime other)
        {
            try
            {
                Anime match = null;

                // Match slug
                if (!string.IsNullOrEmpty(other.Slug))
                {
                    match = await GetAnime(other.Slug);
                    if (match != null) return match;
                }

                // Match title (make sure it's not the same as slug to reduce redundant requests
                if (other.Slug != other.Title)
                {
                    match = await GetAnime(Slugify(other.Title));
                    if (match != null) return match;
                }

                // Match japanese title (if it exists
                if (!string.IsNullOrEmpty(other.TitleJapanese))
                {
                    match = await GetAnime(Slugify(other.TitleJapanese));
                    if (match != null) return match;
                }

                // Match synonyms
                foreach (Synonym synonym in other.Synonyms)
                {
                    match = await GetAnime(Slugify(synonym.Name));
                    if (match != null) return match;
                }

                return null;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
            
        }
        #endregion

        #region Write API
        public override async Task<bool> UpdateAnime(WatchEntry watchEntry)
        {
            if (watchEntry.Anime == null) throw new WatchEntryNotLinkedException(this);
            if (watchEntry.Anime.Api != Id) throw new ApiIncompatibleException(this);
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("auth_token", authToken),
                new KeyValuePair<string, string>("status", WatchStatusCode(watchEntry.Status)),
                new KeyValuePair<string, string>("episodes_watched", watchEntry.Episode.ToString()),
                new KeyValuePair<string, string>("rewatched_times", watchEntry.RewatchCount.ToString()),
                new KeyValuePair<string, string>("rating", (watchEntry.Score / 2).ToString()),
                new KeyValuePair<string, string>("notes", watchEntry.Notes)
            });

            // Run HttpRequest
            string url = URL_ANIME_UPDATE + watchEntry.Anime.ServerId;
            HttpResponseMessage response = await httpClient.PostAsync(url, content);

            // Check for error
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new WriteApiException("Update failed for " + watchEntry.Anime.ServerId, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            return true;
        }

        public override async Task<bool> AddAnime(WatchEntry watchEntry)
        {
            return await UpdateAnime(watchEntry);
        }

        public override async Task<bool> DeleteAnime(string animeId)
        {
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("auth_token", authToken),
            });

            // Run HttpRequest
            string url = URL_ANIME_UPDATE + animeId + "/remove";
            HttpResponseMessage response = await httpClient.PostAsync(url, content);

            // Check for error
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new WriteApiException("Delete failed for " + animeId, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            return true;
        }
        #endregion

        #region Converters
        private AnimeType GetAnimeType(string type)
        {
            if (type == null) return AnimeType.Unknown;

            switch (type)
            {
                case "TV":
                    return AnimeType.TV;

                case "OVA":
                    return AnimeType.OVA;

                case "Special":
                    return AnimeType.Special;

                case "Movie":
                    return AnimeType.Movie;

                case "ONA":
                    return AnimeType.ONA;

                case "Music":
                    return AnimeType.Music;

                default:
                    return AnimeType.Unknown;
            }

        }

        private AirStatus GetAirStatus(string status)
        {
            if (status == null) return AirStatus.Unknown;

            switch (status)
            {
                case "Completed":
                    return AirStatus.Aired;

                case "Currently Airing":
                    return AirStatus.OnGoing;

                case "Not yet aired":
                    return AirStatus.Upcoming;

                default:
                    return AirStatus.Unknown;
            }

        }

        private WatchStatus GetWatchStatus(string status)
        {
            if (status == null) return WatchStatus.Unknown;

            switch (status)
            {
                case "completed":
                    return WatchStatus.Completed;

                case "currently-watching":
                    return WatchStatus.Watching;

                case "on-hold":
                    return WatchStatus.OnHold;

                case "plan-to-watch":
                    return WatchStatus.Planned;

                case "dropped":
                    return WatchStatus.Dropped;

                default:
                    return WatchStatus.Unknown;
            }

        }

        private string WatchStatusCode(WatchStatus status)
        {
            if (status == WatchStatus.Unknown) return null;

            switch (status)
            {
                case WatchStatus.Completed:
                    return "completed";

                case WatchStatus.Watching:
                    return "currently-watching";

                case WatchStatus.OnHold:
                    return "on-hold";

                case WatchStatus.Planned:
                    return "planned";

                case WatchStatus.Dropped:
                    return "dropped";

                default:
                    throw new Exception("Invalid parameter");
            }
        }
        #endregion
    }
}
