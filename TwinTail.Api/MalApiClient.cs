﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using Sylpheed.Helpers;
using System.Diagnostics;

using TwinTail.Entities;
using HtmlAgilityPack;
using HtmlAgilityPack.Samples;

namespace TwinTail.Api
{
    public class MalApiClient : ApiClient
    {
        private static readonly string API_KEY = "api-indiv-3c2464c3bfc32e5bd0f2de90d2c535e6"; //"api-taiga-32864c09ef538453b4d8110734ee355b"; // temporarily borrowing Taiga's for testing
        private static readonly string URL_VERIFY_CREDENTIALS = "http://myanimelist.net/api/account/verify_credentials.xml";
        private static readonly string URL_ANIME_LIST = "http://myanimelist.net/malappinfo.php";
        private static readonly string URL_ANIME_SEARCH = "http://myanimelist.net/api/anime/search.xml";
        private static readonly string URL_ANIME_UPDATE = "http://myanimelist.net/api/animelist/update/";
        private static readonly string URL_ANIME_ADD = "http://myanimelist.net/api/animelist/add/";
        private static readonly string URL_ANIME_DELETE = "http://myanimelist.net/api/animelist/delete/";

        public override string Id
        {
            get
            {
                return "MyAnimeList";
            }
        }

        #region Authentication
        public override async Task Authenticate(string user, string password)
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", StringHelper.Base64Encode(user + ":" + password));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue(new System.Net.Http.Headers.ProductHeaderValue(API_KEY)));

            // Read response
            HttpResponseMessage response = await httpClient.GetAsync(URL_VERIFY_CREDENTIALS);
            if (response.StatusCode != System.Net.HttpStatusCode.OK) throw new AuthenticationFailureException(this);

            User = user;
            Authenticated = true;
        }
        #endregion

        #region Read API
        public override async Task<IEnumerable<WatchEntry>> GetWatchEntries(string user)
        {
            // Run HTTP Request
            string url = URL_ANIME_LIST + "?u=" + user + "&status=all&type=anime";
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for HTTP error
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            string responseString = await response.Content.ReadAsStringAsync();
            List<WatchEntry> list = new List<WatchEntry>();
            XmlDocument root = new XmlDocument();
            root.LoadXml(SanitizeAnimeListXml(responseString));

            // Check if user exists
            if (root.SelectNodes("error").Count > 0) throw new UserNotFoundException(user, this);

            // Parse each xml anime objects
            foreach (XmlNode node in root.SelectSingleNode("myanimelist"))
            {
                if (node.Name == "anime")
                {
                    Anime anime = new Anime();
                    anime.Api = Id;
                    anime.ServerId = node["series_animedb_id"].InnerText;
                    anime.Title = node["series_title"].InnerText;
                    anime.Episodes = int.Parse(node["series_episodes"].InnerText);

                    // Synonyms
                    string[] synonyms = node["series_synonyms"].InnerText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string synonym in synonyms)
                    {
                        anime.Synonyms.Add(new Synonym(synonym.Trim()));
                    }

                    // Type
                    switch (node["series_type"].InnerText)
                    {
                        case "1":
                            anime.Type = AnimeType.TV;
                            break;

                        case "2":
                            anime.Type = AnimeType.OVA;
                            break;

                        case "3":
                            anime.Type = AnimeType.Movie;
                            break;

                        case "4":
                            anime.Type = AnimeType.Special;
                            break;

                        case "5":
                            anime.Type = AnimeType.ONA;
                            break;

                        default:
                            break;
                    }

                    // Air Status
                    switch (node["series_status"].InnerText)
                    {
                        case "1":
                            anime.AirStatus = AirStatus.OnGoing;
                            break;

                        case "2":
                            anime.AirStatus = AirStatus.Aired;
                            break;

                        case "3":
                            anime.AirStatus = AirStatus.Upcoming;
                            break;

                        default:
                            break;
                    }

                    // Date
                    DateTime date;
                    if (DateTime.TryParse(node["series_start"].InnerText, out date)) anime.DateStarted = date;
                    if (DateTime.TryParse(node["series_end"].InnerText, out date)) anime.DateEnded = date;

                    anime.ImageUrl = node["series_image"].InnerText;

                    WatchEntry watchEntry = new WatchEntry();
                    watchEntry.Anime = anime;
                    watchEntry.User = User;
                    watchEntry.Episode = int.Parse(node["my_watched_episodes"].InnerText);
                    if (DateTime.TryParse(node["my_start_date"].InnerText, out date)) watchEntry.DateStarted = date;
                    if (DateTime.TryParse(node["my_finish_date"].InnerText, out date)) watchEntry.DateEnded = date;
                    watchEntry.Score = int.Parse(node["my_score"].InnerText);

                    // Watch status
                    switch (node["my_status"].InnerText)
                    {
                        case "1":
                            watchEntry.Status = WatchStatus.Watching;
                            break;

                        case "2":
                            watchEntry.Status = WatchStatus.Completed;
                            break;

                        case "3":
                            watchEntry.Status = WatchStatus.OnHold;
                            break;

                        case "4":
                            watchEntry.Status = WatchStatus.Dropped;
                            break;

                        case "6":
                            watchEntry.Status = WatchStatus.Planned;
                            break;

                        default:
                            break;
                    }

                    list.Add(watchEntry);
                    AnimeFetched?.Invoke(anime, this);
                }
            }

            return new ReadOnlyCollection<WatchEntry>(list);
        }

        public override async Task<IEnumerable<Anime>> SearchAnime(string query)
        {
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Run HTTP Request
            //string url = URL_ANIME_SEARCH + "?q=" + query;
            //url = Uri.EscapeUriString(url);
            query = Uri.EscapeDataString(query);
            string url = URL_ANIME_SEARCH + "?q=" + query;

            // Read Response
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for errors
            if (response.StatusCode == System.Net.HttpStatusCode.NoContent) throw new SearchNoResultException(query, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            string responseString = await response.Content.ReadAsStringAsync();

            List<Anime> list = new List<Anime>();
            XmlDocument root = new XmlDocument();
            root.LoadXml(StringHelper.SanitizeXml(responseString));

            // Parse each xml anime objects
            foreach (XmlNode node in root.SelectSingleNode("anime"))
            {
                if (node.Name == "entry")
                {
                    Anime anime = new Anime();
                    anime.Api = Id;
                    anime.ServerId = node["id"].InnerText;
                    anime.Title = node["title"].InnerText;
                    anime.TitleEnglish = node["english"].InnerText;
                    anime.Episodes = Int32.Parse(node["episodes"].InnerText);
                    anime.MembersScore = decimal.Parse(node["score"].InnerText);
                    anime.Type = GetAnimeType(node["type"].InnerText);
                    anime.AirStatus = GetAirStatus(node["status"].InnerText);
                    anime.ImageUrl = node["image"].InnerText;

                    // Synopsis
                    anime.Synopsis = HtmlToText.ConvertHtml(node["synopsis"].InnerText);

                    // Synonyms
                    if (node.SelectSingleNode("synonyms") != null)
                    {
                        string[] synonyms = node["synonyms"].InnerText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string synonym in synonyms)
                        {
                            anime.Synonyms.Add(new Synonym(synonym.Trim()));
                        }
                    }

                    // Date
                    DateTime date;
                    if (DateTime.TryParse(node["start_date"].InnerText, out date)) anime.DateStarted = date;
                    if (DateTime.TryParse(node["end_date"].InnerText, out date)) anime.DateEnded = date;

                    list.Add(anime);
                    AnimeFetched?.Invoke(anime, this);
                }
            }

            return new ReadOnlyCollection<Anime>(list);
        }

        public override async Task<Anime> GetAnime(string animeId)
        {
            IEnumerable<Anime> list = await SearchAnime(animeId);

            var query = from anime in list
                         where anime.Title == animeId
                         select anime;

            Anime result = query.FirstOrDefault();
            if (result == null) throw new AnimeNotFoundException(animeId, this);
            AnimeFetched?.Invoke(result, this);

            return result;
        }

        public override async Task<IEnumerable<Anime>> GetDatabase()
        {
            await Task.Delay(0);
            throw new ApiMethodNotSupported(this);
        }

        public override async Task<Anime> Match(Anime other)
        {
            try
            {
                // Match using title
                IEnumerable<Anime> result = await SearchAnime(StringHelper.Simplify(other.Title));
                Anime match = match = TwinTailHelper.MatchSearchResult(result, other);
                if (match != null) return match;

                // Match using japanese title (if it exists)
                if (!string.IsNullOrEmpty(other.TitleJapanese))
                {
                    result = await SearchAnime(StringHelper.Simplify(other.TitleJapanese));
                    match = TwinTailHelper.MatchSearchResult(result, other);
                    if (match != null) return match;
                }

                // Match using synonyms
                foreach (Synonym synonym in other.Synonyms)
                {
                    result = await SearchAnime(StringHelper.Simplify(synonym.Name));
                    match = match = TwinTailHelper.MatchSearchResult(result, other);
                    if (match != null) return match;
                }

                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        #endregion

        #region Write API
        public override async Task<bool> UpdateAnime(WatchEntry watchEntry)
        {
            if (watchEntry.Anime == null) throw new WatchEntryNotLinkedException(this);
            if (watchEntry.Anime.Api != Id) throw new ApiIncompatibleException(this);
            if (!Authenticated) throw new AuthenticationFailureException(this);

            string url = URL_ANIME_UPDATE + watchEntry.Anime.ServerId + ".xml";
            HttpResponseMessage response = await httpClient.PostAsync(url, GeneratePostXml(watchEntry));

            if (!response.IsSuccessStatusCode) throw new WriteApiException("Update failed", this);
            return true;
        }

        public override async Task<bool> AddAnime(WatchEntry watchEntry)
        {
            if (watchEntry.Anime == null) throw new WatchEntryNotLinkedException(this);
            if (watchEntry.Anime.Api != Id) throw new ApiIncompatibleException(this);
            if (!Authenticated) throw new AuthenticationFailureException(this);

            string url = URL_ANIME_ADD + watchEntry.Anime.ServerId + ".xml";
            HttpResponseMessage response = await httpClient.PostAsync(url, GeneratePostXml(watchEntry));

            if (!response.IsSuccessStatusCode) throw new WriteApiException("Update failed", this);
            return true;
        }

        public override async Task<bool> DeleteAnime(string animeId)
        {
            if (!Authenticated) throw new AuthenticationFailureException(this);

            string url = URL_ANIME_DELETE + animeId + ".xml";

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("id", animeId),
            });

            HttpResponseMessage response = await httpClient.PostAsync(url, content);
            if (!response.IsSuccessStatusCode) throw new WriteApiException("Update failed", this);
            return true;
        }
        #endregion

        #region Converters
        private AnimeType GetAnimeType(string type)
        {
            if (type == null) return AnimeType.Unknown;

            switch (type)
            {
                case "TV":
                    return AnimeType.TV;

                case "OVA":
                    return AnimeType.OVA;

                case "Special":
                    return AnimeType.Special;

                case "Movie":
                    return AnimeType.Movie;

                case "ONA":
                    return AnimeType.ONA;

                default:
                    return AnimeType.Doujin;
            }

        }

        private AirStatus GetAirStatus(string status)
        {
            if (status == null) return AirStatus.Unknown;

            switch (status)
            {
                case "finished airing":
                    return AirStatus.Aired;

                case "currently airing":
                    return AirStatus.OnGoing;

                case "not yet aired":
                    return AirStatus.Upcoming;

                default:
                    return AirStatus.Unknown;
            }

        }

        private WatchStatus GetWatchStatus(string status)
        {
            if (status == null) return WatchStatus.Unknown;

            switch (status)
            {
                case "completed":
                    return WatchStatus.Completed;

                case "watching":
                    return WatchStatus.Watching;

                case "on-hold":
                    return WatchStatus.OnHold;

                case "plan to watch":
                    return WatchStatus.Planned;

                case "dropped":
                    return WatchStatus.Dropped;

                default:
                    return WatchStatus.Unknown;
            }

        }

        private string WatchStatusCode(WatchStatus status)
        {
            if (status == WatchStatus.Unknown) return null;

            switch (status)
            {
                case WatchStatus.Completed:
                    return "2";

                case WatchStatus.Watching:
                    return "1";

                case WatchStatus.OnHold:
                    return "3";

                case WatchStatus.Planned:
                    return "6";

                case WatchStatus.Dropped:
                    return "4";

                default:
                    return null;
            }
        }
        #endregion

        private FormUrlEncodedContent GeneratePostXml(WatchEntry watchEntry)
        {
            XDocument doc = new XDocument(new XElement("entry",
                                           new XElement("episode", watchEntry.Episode.ToString()),
                                           new XElement("status", WatchStatusCode(watchEntry.Status)),
                                           new XElement("score", watchEntry.Score.ToString()),
                                           new XElement("downloaded_episode", ""),
                                           new XElement("storage_type", ""),
                                           new XElement("storage_value", ""),
                                           new XElement("times_rewatched", watchEntry.RewatchCount),
                                           new XElement("rewatch_value", watchEntry.RewatchScore),
                                           new XElement("date_start", watchEntry.DateStarted.Value.ToString("mmddyyyy"),
                                           new XElement("date_finish", watchEntry.DateEnded.Value.ToString("mmddyyyy")),
                                           new XElement("priority", ""),
                                           new XElement("comments", watchEntry.Notes)
                                           //new XElement("fansub_group", string.Join<string>(",", userStat.FansubGroups)),
                                           //new XElement("tags", string.Join<string>(",", userStat.Tags))
                                           )));


            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("id", watchEntry.Anime.ServerId),
                new KeyValuePair<string, string>("data", doc.ToString()),
            });

            return content;
        }

        #region XML Cleanup
        private static Lazy<Regex> s_tagElementContentsRegex =
            new Lazy<Regex>(() => new Regex("<my_tags>(?<TagText>.*?)</my_tags>", RegexOptions.Compiled | RegexOptions.CultureInvariant));
        private static Regex TagElementContentsRegex { get { return s_tagElementContentsRegex.Value; } }

        private static Lazy<Regex> s_nonEntityAmpersandRegex =
            new Lazy<Regex>(() => new Regex("&(?!lt;)(?!gt;)(?!amp;)(?!apos;)(?!quot;)(?!#x[0-9a-fA-f]+;)(?!#[0-9]+;)", RegexOptions.Compiled | RegexOptions.CultureInvariant));
        private static Regex NonEntityAmpersandRegex { get { return s_nonEntityAmpersandRegex.Value; } }

        // Remove any code points not in: U+0009, U+000A, U+000D, U+0020–U+D7FF, U+E000–U+FFFD (see http://en.wikipedia.org/wiki/Xml)
        private static Lazy<Regex> s_invalidXmlCharacterRegex =
            new Lazy<Regex>(() => new Regex("[^\\u0009\\u000A\\u000D\\u0020-\\uD7FF\\uE000-\\uFFFD]", RegexOptions.Compiled | RegexOptions.CultureInvariant));
        private static Regex InvalidXmlCharacterRegex { get { return s_invalidXmlCharacterRegex.Value; } }

        private static MatchEvaluator TagElementContentsReplacer = (Match match) =>
        {
            string tagText = match.Groups["TagText"].Value;
            string replacementTagText = NonEntityAmpersandRegex.Replace(tagText, "&amp;");
            replacementTagText = InvalidXmlCharacterRegex.Replace(replacementTagText, "");
            return "<my_tags>" + replacementTagText + "</my_tags>";
        };

        private static string SanitizeAnimeListXml(string rawXml)
        {
            return TagElementContentsRegex.Replace(rawXml, TagElementContentsReplacer);
        }
        #endregion

    }
}
