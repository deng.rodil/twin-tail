﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sylpheed.Helpers;
using TwinTail.Entities;


namespace TwinTail.Api
{
    public class HerroApiClient : ApiClient
    {
        private static readonly string URL_ANIME_LIST = "http://api.herro.co/list/anime/";
        private static readonly string URL_ANIME_SEARCH = "http://api.herro.co/search/anime/";
        private static readonly string URL_ANIME_GET = "http://api.herro.co/anime/";
        private static readonly string URL_ANIME_UPDATE = "http://api.herro.co/list/anime/update";
        private static readonly string URL_ANIME_ADD = "http://api.herro.co/list/anime/add";
        private static readonly string URL_ANIME_DELETE = "http://api.herro.co/list/anime/delete";
        private static readonly string URL_ANIME_ALL = "http://api.herro.co/listall";

        public HerroApiClient()
        {
            ApiType = AnimeApi.Herro;
        }

        #region Authentication
        public override async Task Authenticate(string user, string password)
        {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Authorization", "Basic " + StringHelper.Base64Encode(user.ToLower() + ":" + password));
            User = user;
            Authenticated = true;
            await Task.Delay(0);
        }
        #endregion

        #region Read API
        public override async Task<IEnumerable<Anime>> GetAnimeList(string username)
        {
            List<Anime> list = new List<Anime>();

            // Run HTTP Request
            string url = URL_ANIME_LIST + username.ToLower();
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for errors
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new UserNotFoundException(username, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Read JSON
            string responseString = await response.Content.ReadAsStringAsync();
            JArray values = JArray.Parse(responseString);
            
            // Parse each json anime objects
            foreach (JToken animeToken in values)
            {
                Anime anime = new Anime();
                anime.Api = ApiType;
                anime.ServerId = (string)animeToken["_id"];
                anime.Title = (string)animeToken["series_title"];
                anime.AirStatus = GetAirStatus((string)animeToken["series_status"]);
                anime.Type = GetAnimeType((string)animeToken["series_type"]);
                if (animeToken["series_total"].Type != JTokenType.Null)
                {
                    anime.Episodes = (int)animeToken["series_total"];
                }
                else
                {
                    anime.Episodes = -1;
                }

                WatchEntry watchEntry = new WatchEntry();
                watchEntry.Anime = anime;
                anime.WatchEntry = watchEntry;
                watchEntry.User = User;
                watchEntry.Episode = (int)animeToken["list_progress"];
                watchEntry.Score = (int)animeToken["list_score"];

                switch ((int)animeToken["list_status"])
                {
                    case 1:
                        watchEntry.Status = WatchStatus.Watching;
                        break;

                    case 2:
                        watchEntry.Status = WatchStatus.Completed;
                        break;

                    case 3:
                        watchEntry.Status = WatchStatus.Planned;
                        break;

                    case 4:
                        watchEntry.Status = WatchStatus.OnHold;
                        break;

                    case 5:
                        watchEntry.Status = WatchStatus.Dropped;
                        break;

                    default:
                        break;
                }

                list.Add(anime);
                if (AnimeFetched != null) AnimeFetched(anime, this);
            }

            return new ReadOnlyCollection<Anime>(list);
        }

        public override async Task<IEnumerable<Anime>> SearchAnime(string query)
        {
            // Run HTTP Request
            string url = URL_ANIME_SEARCH + query.ToLower();
            url = Uri.EscapeUriString(url);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            // Check for error
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            string responseString = await response.Content.ReadAsStringAsync();
            JArray values = JArray.Parse(responseString);

            // Parse each json anime objects
            List<Anime> list = new List<Anime>();
            foreach (JToken animeToken in values)
            {
                Anime anime = new Anime();
                anime.Api = ApiType;
                anime.ServerId = (string)animeToken["_id"];
                anime.Title = (string)animeToken["title"];
                anime.Slug = (string)animeToken["slug"];
                anime.ImageUrl = (string)animeToken["image_url"];

                JToken metadata = animeToken["metadata"];
                anime.Type = GetAnimeType((string)metadata["series_type"]);
                anime.Synopsis = (string)metadata["plot"];
                anime.AirStatus = GetAirStatus((string)metadata["series_status"]);

                // Date
                JToken dateStarted = metadata["series_start"];
                if (dateStarted.Type != JTokenType.Null) anime.DateStarted = (DateTime)dateStarted;
                JToken dateEnded = metadata["series_end"];
                if (dateEnded.Type != JTokenType.Null) anime.DateEnded = (DateTime)dateEnded;

                // English Title
                JArray english = (JArray)metadata["title_english"];
                if (english.Count > 0)
                {
                    string val = (string)english[0];
                    if (val.Length > 0) anime.TitleEnglish = val;
                }

                // Synonyms
                JArray synonyms = (JArray)metadata["title_aka"];
                foreach (JToken token in synonyms)
                {
                    string val = (string)token;
                    if (!string.IsNullOrEmpty(val)) anime.Synonyms.Add(new Synonym(val));
                }

                list.Add(anime);
                if (AnimeFetched != null) AnimeFetched(anime, this);
            }

            // Check if there's a result
            if (list.Count == 0) throw new SearchNoResultException(query, this);

            return new ReadOnlyCollection<Anime>(list);;
        }

        public override async Task<Anime> GetAnime(string animeId)
        {
            string url = URL_ANIME_GET + animeId;
            HttpResponseMessage response = await httpClient.GetAsync(url);
            
            // Check for errors
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError) throw new AnimeNotFoundException(animeId, this);
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);
 
            // Get json
            string responseString = await response.Content.ReadAsStringAsync();
            JObject json = JObject.Parse(responseString);

            Anime anime = new Anime();
            anime.Api = ApiType;
            anime.ServerId = animeId;
            anime.Title = (string)json["title"];
            anime.Slug = (string)json["slug"];
            anime.ImageUrl = (string)json["image_url"];

            JToken metadata = json["metadata"];
            anime.Synopsis = (string)metadata["plot"];
            anime.AirStatus = GetAirStatus((string)metadata["series_status"]);
            anime.Type = GetAnimeType((string)metadata["series_type"]);

            // Episode
            if (metadata["episodes_total"].Type != JTokenType.Null)
            {
                anime.Episodes = (int)metadata["episodes_total"];
            }
            else
            {
                anime.Episodes = -1;
            }

            JToken episodeLength = metadata["episodes_length"];
            if (episodeLength.Type != JTokenType.Null) anime.EpisodeLength = (int)episodeLength;

            // Date
            JToken dateStarted = metadata["series_start"];
            if (dateStarted.Type != JTokenType.Null) anime.DateStarted = (DateTime)dateStarted;
            JToken dateEnded = metadata["series_end"];
            if (dateEnded.Type != JTokenType.Null) anime.DateEnded = (DateTime)dateEnded;

            // Genre
            JArray genres = (JArray)metadata["genres"];
            foreach (JToken token in genres){
                string val = (string)token;
                if (val.Length > 0) anime.Genres.Add(new Genre(val));
            }

            // English Title
            JArray english = (JArray)metadata["title_english"];
            if (english.Count > 0){
                string val = (string)english[0];
                if (!string.IsNullOrEmpty(val)) anime.TitleEnglish = val;
            }

            // Synonyms
            JArray synonyms = (JArray)metadata["title_aka"];
            foreach (JToken token in synonyms){
                string val = (string)token;
                if (!string.IsNullOrEmpty(val)) anime.Synonyms.Add(new Synonym(val));
            }

            if (AnimeFetched != null) AnimeFetched(anime, this);
            return anime;
        }

        public override async Task<IEnumerable<Anime>> GetDatabase()
        {
            // Run HTTP Request
            HttpResponseMessage response = await httpClient.GetAsync(URL_ANIME_ALL);

            // Check for error
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            string responseString = await response.Content.ReadAsStringAsync();
            JArray values = JArray.Parse(responseString);

            // Parse each json anime objects
            List<Anime> list = new List<Anime>();
            foreach (JToken animeToken in values)
            {
                Anime anime = new Anime();
                anime.Api = ApiType;
                anime.ServerId = (string)animeToken["_id"];

                // Ignore invalid anime ids
                if (anime.ServerId.Length == 0) continue;

                anime.Title = (string)animeToken["title"];
                anime.Slug = (string)animeToken["slug"];
                anime.ImageUrl = "http://herro.co/static/series/anime/" + (string)animeToken["imageId"] + ".jpg";

                JToken metadata = animeToken["metadata"];
                // English Title
                JToken english = metadata["english"];
                if (english != null && english.Type != JTokenType.Null)
                {
                    foreach (JToken token in english)
                    {
                        string val = (string)token;
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (anime.TitleEnglish == null) anime.TitleEnglish = val;
                            else anime.Synonyms.Add(new Synonym(val));
                        }
                    }
                }
                
                // Synonyms
                JToken synonyms = metadata["aka"];
                if (synonyms != null && synonyms.Type != JTokenType.Null)
                {
                    foreach (JToken token in synonyms)
                    {
                        string val = (string)token;
                        if (!string.IsNullOrEmpty(val)) anime.Synonyms.Add(new Synonym(val));
                    }
                }
                

                list.Add(anime);
                if (AnimeFetched != null) AnimeFetched(anime, this);
            }

            return new ReadOnlyCollection<Anime>(list);
        }

        public override async Task<Anime> Match(Anime other)
        {
            try
            {
                // Match using title
                IEnumerable<Anime> result = await SearchAnime(StringHelper.Simplify(other.Title));
                Anime match = match = TwinTailHelper.MatchSearchResult(result, other);
                if (match != null) return match;

                // Match using japanese title (if it exists)
                if (string.IsNullOrEmpty(other.TitleJapanese))
                {
                    result = await SearchAnime(StringHelper.Simplify(other.TitleJapanese));
                    match = TwinTailHelper.MatchSearchResult(result, other);
                    if (match != null) return match;
                }

                // Match using synonyms
                foreach (Synonym synonym in other.Synonyms)
                {
                    result = await SearchAnime(StringHelper.Simplify(synonym.Name));
                    match = match = TwinTailHelper.MatchSearchResult(result, other);
                    if (match != null) return match;
                }

                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
            
        }

        #endregion

        #region Write API
        public override async Task<bool> UpdateAnime(WatchEntry watchEntry)
        {
            if (watchEntry.Anime == null) throw new WatchEntryNotLinkedException(this);
            if (watchEntry.Anime.Api != ApiType) throw new ApiIncompatibleException(this);
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("_id", watchEntry.Anime.ServerId),
                new KeyValuePair<string, string>("status", WatchStatusCode(watchEntry.Status)),
                new KeyValuePair<string, string>("progress", watchEntry.Episode.ToString()),
                new KeyValuePair<string, string>("score", watchEntry.Score.ToString())
            });

            // Read response
            HttpResponseMessage response = await httpClient.PostAsync(URL_ANIME_UPDATE, content);

            // Check for error
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                string s = await response.Content.ReadAsStringAsync();
                JObject j = JObject.Parse(s);
                string msg = (string)j["response"];
                if (msg.Trim() == "Bad authentication data")
                {
                    Authenticated = false;
                    throw new AuthenticationFailureException(this);
                }
                else
                {
                    throw new WriteApiException((string)j["response"], this);
                }
            }
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Parse json response
            string responseString = await response.Content.ReadAsStringAsync();
            JObject json = JObject.Parse(responseString);
            
            // Check if POST data is valid
            return (string)json["status"] != "OK";
        }

        public override async Task<bool> AddAnime(WatchEntry watchEntry)
        {
            if (watchEntry.Anime == null) throw new WatchEntryNotLinkedException(this);
            if (watchEntry.Anime.Api != ApiType) throw new ApiIncompatibleException(this);
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("_id", watchEntry.Anime.ServerId),
                new KeyValuePair<string, string>("status", WatchStatusCode(watchEntry.Status)),
                new KeyValuePair<string, string>("progress", watchEntry.Episode.ToString()),
                new KeyValuePair<string, string>("score", watchEntry.Score.ToString())
            });

            // Read response
            HttpResponseMessage response = await httpClient.PostAsync(URL_ANIME_ADD, content);

            // Check for error
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                string s = await response.Content.ReadAsStringAsync();
                JObject j = JObject.Parse(s);
                string msg = (string)j["response"];
                if (msg.Trim() == "Bad authentication data")
                {
                    Authenticated = false;
                    throw new AuthenticationFailureException(this);
                }
                else
                {
                    throw new WriteApiException((string)j["response"], this);
                }
            }
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Parse json response
            string responseString = await response.Content.ReadAsStringAsync();
            JObject json = JObject.Parse(responseString);

            // Check if POST data is valid
            return (string)json["status"] != "OK";
        }

        public override async Task<bool> DeleteAnime(string animeId)
        {
            if (!Authenticated) throw new AuthenticationFailureException(this);

            // Set POST data
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("_id", animeId)
            });

            HttpResponseMessage response = await httpClient.PostAsync(URL_ANIME_DELETE, content);

            // Check for error
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                string s = await response.Content.ReadAsStringAsync();
                JObject j = JObject.Parse(s);
                string msg = (string)j["response"];
                if (msg.Trim() == "Bad authentication data")
                {
                    Authenticated = false;
                    throw new AuthenticationFailureException(this);
                }
                else
                {
                    throw new WriteApiException((string)j["response"], this);
                }
            }
            if (!response.IsSuccessStatusCode) throw new Exception(response.ReasonPhrase);

            // Parse json response
            string responseString = await response.Content.ReadAsStringAsync();
            JObject json = JObject.Parse(responseString);

            // Check if POST data is valid
            return (string)json["status"] != "OK";
        }
        #endregion

        #region Converters
        private AnimeType GetAnimeType(string type)
        {
            if (type == null) return AnimeType.Unknown;

            switch (type)
            {
                case "TV":
                    return AnimeType.TV;

                case "OVA":
                    return AnimeType.OVA;

                case "Special":
                    return AnimeType.Special;

                case "Movie":
                    return AnimeType.Movie;

                case "ONA":
                    return AnimeType.ONA;

                case "Music":
                    return AnimeType.Music;

                default:
                    return AnimeType.Unknown;
            }

        }

        private AirStatus GetAirStatus(string status)
        {
            if (status == null) return AirStatus.Unknown;

            switch (status)
            {
                case "Completed":
                    return AirStatus.Aired;

                case "Airing":
                    return AirStatus.OnGoing;

                case "Not yet aired":
                    return AirStatus.Upcoming;

                default:
                    return AirStatus.Unknown;
            }

        }

        private WatchStatus GetWatchStatus(string status)
        {
            if (status == null) return WatchStatus.Unknown;

            switch (status)
            {
                case "2":
                    return WatchStatus.Completed;

                case "1":
                    return WatchStatus.Watching;

                case "3":
                    return WatchStatus.OnHold;

                case "4":
                    return WatchStatus.Planned;

                case "5":
                    return WatchStatus.Dropped;

                default:
                    return WatchStatus.Unknown;
            }

        }

        private string WatchStatusCode(WatchStatus status)
        {
            if (status == WatchStatus.Unknown) return null;

            switch (status)
            {
                case WatchStatus.Completed:
                    return "2";

                case WatchStatus.Watching:
                    return "1";

                case WatchStatus.Planned:
                    return "3";

                case WatchStatus.OnHold:
                    return "4";

                case WatchStatus.Dropped:
                    return "5";

                default:
                    return null;
            }
        }
        #endregion

    }
}
