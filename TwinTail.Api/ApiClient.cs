﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Sylpheed.Helpers;
using TwinTail.Entities;

namespace TwinTail.Api
{
    public abstract class ApiClient
    {
        public abstract string Id { get; }
        public string User { get; protected set; }
        public bool Authenticated { get; protected set; }

        protected HttpClient httpClient;
        public Action<Anime, ApiClient> AnimeFetched;

        public ApiClient()
        {
            httpClient = new HttpClient();
        }

        #region Authentication
        public abstract Task Authenticate(string user, string password);
        #endregion

        #region Read API
        public abstract Task<IEnumerable<WatchEntry>> GetWatchEntries(string user);
        public virtual async Task<IEnumerable<WatchEntry>> GetWatchEntries()
        {
            return await GetWatchEntries(User);
        }
        public abstract Task<IEnumerable<Anime>> SearchAnime(string query);
        public abstract Task<Anime> GetAnime(string animeId);
        public abstract Task<IEnumerable<Anime>> GetDatabase();
        
        public abstract Task<Anime> Match(Anime other);
        #endregion

        #region Write API
        public abstract Task<bool> UpdateAnime(WatchEntry userEntry);
        public abstract Task<bool> AddAnime(WatchEntry userEntry);
        public abstract Task<bool> DeleteAnime(string animeId);
        #endregion

        public static string Slugify(string title)
        {
            title = StringHelper.Simplify(title);
            title = title.Replace(' ', '-');
            return title;
        }

    }

    #region Exceptions
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string user, ApiClient api)
            : base(string.Format("[{0}] User: {1} does not exist", api.Id, user))
        {
        }
    }

    public class SearchNoResultException : Exception
    {
        public SearchNoResultException(string query, ApiClient api)
            : base(string.Format("[{0}] No anime found for query: {1}", api.Id.ToString(), query))
        {
        }
    }

    public class AnimeNotFoundException : Exception
    {
        public AnimeNotFoundException(string animeId, ApiClient api)
            : base(string.Format("[{0}] No anime found for id: {1}", api.Id.ToString(), animeId))
        {
        }
    }

    public class AuthenticationFailureException : Exception
    {
        public AuthenticationFailureException(ApiClient api)
            : base("[" + api.Id + "] API session failed to authenticate")
        {
        }
    }

    public class WriteApiException : Exception
    {
        public WriteApiException(string message, ApiClient api)
            : base(string.Format("[{0}] {1}", api.Id, message))
        {
        }
    }

    public class ApiMethodNotSupported : Exception
    {
        public ApiMethodNotSupported(ApiClient api)
            : base(string.Format("[{0}] API method is not supported", api.Id))
        {
        }
    }

    public class ApiIncompatibleException : Exception
    {
        public ApiIncompatibleException(ApiClient api)
            : base(string.Format("Trying to use a different source API on {0}", api.Id))
        {
        }
    }

    public class WatchEntryNotLinkedException : Exception
    {
        public WatchEntryNotLinkedException(ApiClient api)
            : base(string.Format("[{0}] Trying to pass WatchEntry object without a linked Anime.", api.Id))
        {
        }
    }
    #endregion
}
