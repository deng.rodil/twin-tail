﻿using Sylpheed.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;

namespace TwinTail.Api
{
    public static class TwinTailHelper
    {
        public static Anime MatchSearchResult(IEnumerable<Anime> list, Anime toMatch)
        {
            if (!toMatch.ReferenceMatchable()) return null;

            var query = from anime in list
                        where (anime.Season==toMatch.Season && anime.Year==toMatch.Year && anime.Type==toMatch.Type)
                        select anime;
            IList<Anime> result = query.ToList();
            // Return immediately if there's only one or zero result
            if (result.Count == 0) return null;
            if (result.Count == 1) return result.First();

            // Get the anime with highest title relevance
            Anime closest = null;
            double highestRelevance = 0;
            foreach (Anime anime in result)
            {
                if (closest == null)
                {
                    closest = anime;
                    highestRelevance = StringHelper.LevenshteinPercentage(toMatch.Title, anime.Title);
                    continue;
                }

                double relevance = StringHelper.LevenshteinPercentage(toMatch.Title, anime.Title);
                if (relevance > highestRelevance)
                {
                    highestRelevance = relevance;
                    closest = anime;
                }
            }

            return closest;
        }
    }
}
