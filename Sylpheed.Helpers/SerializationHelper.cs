﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sylpheed.Helpers
{
    public static class SerializationHelper
    {
        public static async Task SerializeToFile(Object obj, FileInfo dest)
        {
            FileInfo tmp = new FileInfo(Path.GetTempFileName());
            using (FileStream stream = tmp.OpenWrite())
            {
                string json = await JsonConvert.SerializeObjectAsync(obj);
                byte[] data = Encoding.UTF8.GetBytes(json);
                await stream.WriteAsync(data, 0, data.Length);
            }

            tmp.CopyTo(dest.FullName, true);
            tmp.Delete();
        }

        public static async Task SerializeToFile(Object obj, FileInfo dest, FileInfo tmp)
        {
            if (tmp.Exists) tmp.Delete();

            using (FileStream stream = tmp.Create())
            {
                string json = await JsonConvert.SerializeObjectAsync(obj);
                byte[] data = Encoding.UTF8.GetBytes(json);
                await stream.WriteAsync(data, 0, data.Length);
            }

            tmp.CopyTo(dest.FullName, true);
            tmp.Delete();
        }

        public static async Task<T> Deserialize<T>(FileInfo file)
        {
            using (StreamReader reader = file.OpenText())
            {
                string json = await reader.ReadToEndAsync();
                T obj = await JsonConvert.DeserializeObjectAsync<T>(json);
                return obj;
            }
        }
    }
}
