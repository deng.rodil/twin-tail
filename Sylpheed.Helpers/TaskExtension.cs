﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sylpheed.Helpers
{
    public static class TaskExtension
    {
        public static void Forget(this Task task)
        {
            Task.Run(() => task)
                    .ContinueWith(t =>
                    {
                        var aggregateException = t.Exception;
                        var flatAe = aggregateException.Flatten();
                    }, TaskContinuationOptions.OnlyOnFaulted);
        }
    }
}
