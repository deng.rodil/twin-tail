﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sylpheed.Helpers
{
    public static class EnumerableExtension
    {
        public static IEnumerable<T> Except<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            List<T> list = items.ToList();
            foreach (T i in other)
            {
                foreach (T j in items)
                {
                    if (getKey(i).Equals(getKey(j)))
                    {
                        list.Remove(j);
                    }
                }
            }

            return list;
        }

        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (T item in items)
            {
                action(item);
            }
        }
    }
}
