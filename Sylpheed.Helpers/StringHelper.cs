﻿using Sgml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Sylpheed.Helpers
{
    public static class StringHelper
    {
        public static string Base64Encode(string s)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(s);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string encoded)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(encoded);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        /// <summary>Computes the Levenshtein Edit Distance between two enumerables.</summary>
        /// <typeparam name="T">The type of the items in the enumerables.</typeparam>
        /// <param name="x">The first enumerable.</param>
        /// <param name="y">The second enumerable.</param>
        /// <returns>The edit distance.</returns>
        private static int LevenshteinDistance<T>(IEnumerable<T> x, IEnumerable<T> y) 
            where T : IEquatable<T>
        {
            // Validate parameters
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");

            // Convert the parameters into IList instances
            // in order to obtain indexing capabilities
            IList<T> first = x as IList<T> ?? new List<T>(x);
            IList<T> second = y as IList<T> ?? new List<T>(y);

            // Get the length of both.  If either is 0, return
            // the length of the other, since that number of insertions
            // would be required.
            int n = first.Count, m = second.Count;
            if (n == 0) return m;
            if (m == 0) return n;

            // Rather than maintain an entire matrix (which would require O(n*m) space),
            // just store the current row and the next row, each of which has a length m+1,
            // so just O(m) space. Initialize the current row.
            int curRow = 0, nextRow = 1;
            int[][] rows = new int[][] { new int[m + 1], new int[m + 1] };
            for (int j = 0; j <= m; ++j) rows[curRow][j] = j;

            // For each virtual row (since we only have physical storage for two)
            for (int i = 1; i <= n; ++i)
            {
                // Fill in the values in the row
                rows[nextRow][0] = i;
                for (int j = 1; j <= m; ++j)
                {
                    int dist1 = rows[curRow][j] + 1;
                    int dist2 = rows[nextRow][j - 1] + 1;
                    int dist3 = rows[curRow][j - 1] +
                        (first[i - 1].Equals(second[j - 1]) ? 0 : 1);

                    rows[nextRow][j] = Math.Min(dist1, Math.Min(dist2, dist3));
                }

                // Swap the current and next rows
                if (curRow == 0)
                {
                    curRow = 1;
                    nextRow = 0;
                }
                else
                {
                    curRow = 0;
                    nextRow = 1;
                }
            }

            // Return the computed edit distance
            return rows[curRow][m];
        }

        public static int LevenshteinDistance(string source, string target)
        {
            return LevenshteinDistance<Char>(source.ToCharArray(), target.ToCharArray());
        }

        public static double LevenshteinPercentage(string source, string target)
        {
            int dist = LevenshteinDistance(source.ToLower(), target.ToLower());
            return 1 - ((double)dist / Math.Max(source.Length, target.Length));
        }


        public static bool FastLevenshtein(string s1, string s2, double percentMatch)
        {
            s1 = s1.ToLower();
            s2 = s2.ToLower();
            int maxDistance = Math.Min(s1.Length - (int)(s1.Length * percentMatch), s2.Length - (int)(s2.Length * percentMatch));

            int nDiagonal = s1.Length - Math.Min(s1.Length, s2.Length);
            int mDiagonal = s2.Length - Math.Min(s1.Length, s2.Length);

            if (s1.Length == 0) return s2.Length <= maxDistance;
            if (s2.Length == 0) return s1.Length <= maxDistance;

            int[,] matrix = new int[s1.Length + 1, s2.Length + 1];

            for (int i = 0; i <= s1.Length; matrix[i, 0] = i++) ;
            for (int j = 0; j <= s2.Length; matrix[0, j] = j++) ;

            int cost;

            for (int i = 1; i <= s1.Length; i++)
            {
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s2.Substring(j - 1, 1) == s1.Substring(i - 1, 1))
                    {
                        cost = 0;
                    }
                    else
                    {
                        cost = 1;
                    }

                    int valueAbove = matrix[i - 1, j];
                    int valueLeft = matrix[i, j - 1] + 1;
                    int valueAboveLeft = matrix[i - 1, j - 1];
                    matrix[i, j] = Math.Min(valueAbove + 1, Math.Min(valueLeft + 1, valueAboveLeft + cost));
                }

                if (i >= nDiagonal)
                {
                    if (matrix[nDiagonal, mDiagonal] > maxDistance)
                    {
                        return false;
                    }
                    else
                    {
                        nDiagonal++;
                        mDiagonal++;
                    }
                }
            }

            return true;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            str = Regex.Replace(str, "[^a-zA-Z0-9% ._]", string.Empty, RegexOptions.Compiled);
            str = Regex.Replace(str, "[ ]{2,}", " ", RegexOptions.Compiled);
            return str;
        }
        
        public static string Simplify(string s)
        {
            return RemoveSpecialCharacters(s).ToLower();
        }

        public static Stream StringToStream(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }

        public static string SanitizeXml(string webResponse)
        {
            StringWriter sWriter = new StringWriter();
            XmlWriter xmlWriter = XmlWriter.Create(sWriter);
            SgmlReader sgmlReader = new SgmlReader();
            sgmlReader.DocType = "HTML";
            //sgmlReader.WhitespaceHandling = WhitespaceHandling.All;
            sgmlReader.CaseFolding = CaseFolding.ToLower;
            sgmlReader.InputStream = new StringReader(webResponse);
            sgmlReader.IgnoreDtd = true;
            while (!sgmlReader.EOF)
            {
                xmlWriter.WriteNode(sgmlReader, true);
            }
            xmlWriter.Flush();
            return sWriter.ToString();
        }

        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Null or empty string");
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }
    }
}
