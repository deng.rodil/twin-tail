
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server Compact Edition
-- --------------------------------------------------
-- Date Created: 04/22/2016 18:45:11
-- Generated from EDMX file: C:\Users\sylph\Documents\Git\twin-tail\TwinTail.Entities\TwinTail.edmx
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------

    ALTER TABLE [AnimeGenre] DROP CONSTRAINT [FK_AnimeGenre_Anime];
GO
    ALTER TABLE [AnimeGenre] DROP CONSTRAINT [FK_AnimeGenre_Genre];
GO
    ALTER TABLE [Synonyms] DROP CONSTRAINT [FK_SynonymAnime];
GO
    ALTER TABLE [Animes] DROP CONSTRAINT [FK_AnimeReference];
GO
    ALTER TABLE [FansubArchiveEntry] DROP CONSTRAINT [FK_FansubArchiveEntry_Fansub];
GO
    ALTER TABLE [FansubArchiveEntry] DROP CONSTRAINT [FK_FansubArchiveEntry_ArchiveEntry];
GO
    ALTER TABLE [FansubWatchEntry] DROP CONSTRAINT [FK_FansubWatchEntry_Fansub];
GO
    ALTER TABLE [FansubWatchEntry] DROP CONSTRAINT [FK_FansubWatchEntry_WatchEntry];
GO
    ALTER TABLE [WatchEntries] DROP CONSTRAINT [FK_WatchEntryAnime];
GO
    ALTER TABLE [ArchiveEntries] DROP CONSTRAINT [FK_ArchiveEntryAnime];
GO
    ALTER TABLE [ArchiveEntries] DROP CONSTRAINT [FK_AccountArchiveEntry];
GO
    ALTER TABLE [WatchEntries] DROP CONSTRAINT [FK_AccountWatchEntry];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- NOTE: if the table does not exist, an ignorable error will be reported.
-- --------------------------------------------------

    DROP TABLE [Animes];
GO
    DROP TABLE [Genres];
GO
    DROP TABLE [Synonyms];
GO
    DROP TABLE [References];
GO
    DROP TABLE [WatchEntries];
GO
    DROP TABLE [ArchiveEntries];
GO
    DROP TABLE [Fansubs];
GO
    DROP TABLE [ApiServices];
GO
    DROP TABLE [Accounts];
GO
    DROP TABLE [AnimeGenre];
GO
    DROP TABLE [FansubArchiveEntry];
GO
    DROP TABLE [FansubWatchEntry];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Animes'
CREATE TABLE [Animes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ServerId] nvarchar(4000)  NOT NULL,
    [Api] nvarchar(4000)  NOT NULL,
    [Slug] nvarchar(4000)  NULL,
    [Title] nvarchar(4000)  NOT NULL,
    [TitleEnglish] nvarchar(4000)  NULL,
    [TitleJapanese] nvarchar(4000)  NULL,
    [Type] int  NOT NULL,
    [Synopsis] nvarchar(4000)  NULL,
    [Episodes] int  NULL,
    [EpisodeLength] int  NULL,
    [AirStatus] int  NOT NULL,
    [DateStarted] datetime  NULL,
    [DateEnded] datetime  NULL,
    [ImageUrl] nvarchar(4000)  NULL,
    [Classification] nvarchar(4000)  NULL,
    [Rank] int  NULL,
    [PopularityRank] int  NULL,
    [MembersScore] decimal(3,2)  NULL,
    [MembersCount] int  NULL,
    [FavoritedCount] int  NULL,
    [DateUpdated] datetime  NULL,
    [Reference_Id] int  NULL
);
GO

-- Creating table 'Genres'
CREATE TABLE [Genres] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(4000)  NOT NULL,
    [Description] nvarchar(4000)  NULL
);
GO

-- Creating table 'Synonyms'
CREATE TABLE [Synonyms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(4000)  NOT NULL,
    [Anime_Id] int  NOT NULL
);
GO

-- Creating table 'References'
CREATE TABLE [References] (
    [Id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'WatchEntries'
CREATE TABLE [WatchEntries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [User] nvarchar(4000)  NOT NULL,
    [Status] int  NOT NULL,
    [Episode] int  NOT NULL,
    [Score] decimal(18,0)  NOT NULL,
    [Rewatching] bit  NOT NULL,
    [RewatchCount] int  NOT NULL,
    [RewatchScore] decimal(18,0)  NOT NULL,
    [Notes] nvarchar(4000)  NULL,
    [DateStarted] datetime  NULL,
    [DateEnded] datetime  NULL,
    [DateUpdated] datetime  NULL,
    [Anime_Id] int  NOT NULL,
    [Account_Id] int  NOT NULL
);
GO

-- Creating table 'ArchiveEntries'
CREATE TABLE [ArchiveEntries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Status] int  NOT NULL,
    [Source] int  NOT NULL,
    [VideoWidth] int  NOT NULL,
    [VideoHeight] int  NOT NULL,
    [IsTemporary] bit  NOT NULL,
    [Notes] nvarchar(4000)  NULL,
    [DateUpdated] datetime  NULL,
    [Anime_Id] int  NOT NULL,
    [Account_Id] int  NOT NULL
);
GO

-- Creating table 'Fansubs'
CREATE TABLE [Fansubs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ShortName] nvarchar(4000)  NULL,
    [FullName] nvarchar(4000)  NULL,
    [Url] nvarchar(4000)  NULL
);
GO

-- Creating table 'ApiServices'
CREATE TABLE [ApiServices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(4000)  NOT NULL,
    [Url] nvarchar(4000)  NULL,
    [Enabled] bit  NOT NULL
);
GO

-- Creating table 'Accounts'
CREATE TABLE [Accounts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Api] nvarchar(4000)  NOT NULL,
    [Username] nvarchar(4000)  NOT NULL,
    [Password] nvarchar(4000)  NOT NULL
);
GO

-- Creating table 'AnimeGenre'
CREATE TABLE [AnimeGenre] (
    [Animes_Id] int  NOT NULL,
    [Genres_Id] int  NOT NULL
);
GO

-- Creating table 'FansubArchiveEntry'
CREATE TABLE [FansubArchiveEntry] (
    [Fansubs_Id] int  NOT NULL,
    [ArchiveEntries_Id] int  NOT NULL
);
GO

-- Creating table 'FansubWatchEntry'
CREATE TABLE [FansubWatchEntry] (
    [Fansubs_Id] int  NOT NULL,
    [WatchEntries_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Animes'
ALTER TABLE [Animes]
ADD CONSTRAINT [PK_Animes]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'Genres'
ALTER TABLE [Genres]
ADD CONSTRAINT [PK_Genres]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'Synonyms'
ALTER TABLE [Synonyms]
ADD CONSTRAINT [PK_Synonyms]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'References'
ALTER TABLE [References]
ADD CONSTRAINT [PK_References]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'WatchEntries'
ALTER TABLE [WatchEntries]
ADD CONSTRAINT [PK_WatchEntries]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'ArchiveEntries'
ALTER TABLE [ArchiveEntries]
ADD CONSTRAINT [PK_ArchiveEntries]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'Fansubs'
ALTER TABLE [Fansubs]
ADD CONSTRAINT [PK_Fansubs]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'ApiServices'
ALTER TABLE [ApiServices]
ADD CONSTRAINT [PK_ApiServices]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'Accounts'
ALTER TABLE [Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Animes_Id], [Genres_Id] in table 'AnimeGenre'
ALTER TABLE [AnimeGenre]
ADD CONSTRAINT [PK_AnimeGenre]
    PRIMARY KEY ([Animes_Id], [Genres_Id] );
GO

-- Creating primary key on [Fansubs_Id], [ArchiveEntries_Id] in table 'FansubArchiveEntry'
ALTER TABLE [FansubArchiveEntry]
ADD CONSTRAINT [PK_FansubArchiveEntry]
    PRIMARY KEY ([Fansubs_Id], [ArchiveEntries_Id] );
GO

-- Creating primary key on [Fansubs_Id], [WatchEntries_Id] in table 'FansubWatchEntry'
ALTER TABLE [FansubWatchEntry]
ADD CONSTRAINT [PK_FansubWatchEntry]
    PRIMARY KEY ([Fansubs_Id], [WatchEntries_Id] );
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Animes_Id] in table 'AnimeGenre'
ALTER TABLE [AnimeGenre]
ADD CONSTRAINT [FK_AnimeGenre_Anime]
    FOREIGN KEY ([Animes_Id])
    REFERENCES [Animes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Genres_Id] in table 'AnimeGenre'
ALTER TABLE [AnimeGenre]
ADD CONSTRAINT [FK_AnimeGenre_Genre]
    FOREIGN KEY ([Genres_Id])
    REFERENCES [Genres]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AnimeGenre_Genre'
CREATE INDEX [IX_FK_AnimeGenre_Genre]
ON [AnimeGenre]
    ([Genres_Id]);
GO

-- Creating foreign key on [Anime_Id] in table 'Synonyms'
ALTER TABLE [Synonyms]
ADD CONSTRAINT [FK_SynonymAnime]
    FOREIGN KEY ([Anime_Id])
    REFERENCES [Animes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SynonymAnime'
CREATE INDEX [IX_FK_SynonymAnime]
ON [Synonyms]
    ([Anime_Id]);
GO

-- Creating foreign key on [Reference_Id] in table 'Animes'
ALTER TABLE [Animes]
ADD CONSTRAINT [FK_AnimeReference]
    FOREIGN KEY ([Reference_Id])
    REFERENCES [References]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AnimeReference'
CREATE INDEX [IX_FK_AnimeReference]
ON [Animes]
    ([Reference_Id]);
GO

-- Creating foreign key on [Fansubs_Id] in table 'FansubArchiveEntry'
ALTER TABLE [FansubArchiveEntry]
ADD CONSTRAINT [FK_FansubArchiveEntry_Fansub]
    FOREIGN KEY ([Fansubs_Id])
    REFERENCES [Fansubs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ArchiveEntries_Id] in table 'FansubArchiveEntry'
ALTER TABLE [FansubArchiveEntry]
ADD CONSTRAINT [FK_FansubArchiveEntry_ArchiveEntry]
    FOREIGN KEY ([ArchiveEntries_Id])
    REFERENCES [ArchiveEntries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FansubArchiveEntry_ArchiveEntry'
CREATE INDEX [IX_FK_FansubArchiveEntry_ArchiveEntry]
ON [FansubArchiveEntry]
    ([ArchiveEntries_Id]);
GO

-- Creating foreign key on [Fansubs_Id] in table 'FansubWatchEntry'
ALTER TABLE [FansubWatchEntry]
ADD CONSTRAINT [FK_FansubWatchEntry_Fansub]
    FOREIGN KEY ([Fansubs_Id])
    REFERENCES [Fansubs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [WatchEntries_Id] in table 'FansubWatchEntry'
ALTER TABLE [FansubWatchEntry]
ADD CONSTRAINT [FK_FansubWatchEntry_WatchEntry]
    FOREIGN KEY ([WatchEntries_Id])
    REFERENCES [WatchEntries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FansubWatchEntry_WatchEntry'
CREATE INDEX [IX_FK_FansubWatchEntry_WatchEntry]
ON [FansubWatchEntry]
    ([WatchEntries_Id]);
GO

-- Creating foreign key on [Anime_Id] in table 'WatchEntries'
ALTER TABLE [WatchEntries]
ADD CONSTRAINT [FK_WatchEntryAnime]
    FOREIGN KEY ([Anime_Id])
    REFERENCES [Animes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WatchEntryAnime'
CREATE INDEX [IX_FK_WatchEntryAnime]
ON [WatchEntries]
    ([Anime_Id]);
GO

-- Creating foreign key on [Anime_Id] in table 'ArchiveEntries'
ALTER TABLE [ArchiveEntries]
ADD CONSTRAINT [FK_ArchiveEntryAnime]
    FOREIGN KEY ([Anime_Id])
    REFERENCES [Animes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ArchiveEntryAnime'
CREATE INDEX [IX_FK_ArchiveEntryAnime]
ON [ArchiveEntries]
    ([Anime_Id]);
GO

-- Creating foreign key on [Account_Id] in table 'ArchiveEntries'
ALTER TABLE [ArchiveEntries]
ADD CONSTRAINT [FK_AccountArchiveEntry]
    FOREIGN KEY ([Account_Id])
    REFERENCES [Accounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountArchiveEntry'
CREATE INDEX [IX_FK_AccountArchiveEntry]
ON [ArchiveEntries]
    ([Account_Id]);
GO

-- Creating foreign key on [Account_Id] in table 'WatchEntries'
ALTER TABLE [WatchEntries]
ADD CONSTRAINT [FK_AccountWatchEntry]
    FOREIGN KEY ([Account_Id])
    REFERENCES [Accounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountWatchEntry'
CREATE INDEX [IX_FK_AccountWatchEntry]
ON [WatchEntries]
    ([Account_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------