﻿using Sylpheed.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;

namespace TwinTail.Entities
{
    public partial class Anime
    {
        [JsonIgnore]
        public Season Season
        {
            get
            {
                if (DateStarted == null) return Season.Unknown;

                float days = (DateStarted.Value.Month * 30) + DateStarted.Value.Day;
                float portion = days / 365.0f;

                if (portion < 0.25f) return Season.Winter;
                else if (portion < 0.5f) return Season.Spring;
                else if (portion < 0.75f) return Season.Summer;
                else return Season.Fall;
            }
        }

        [JsonIgnore]
        public int Year
        {
            get
            {
                if (DateStarted != null) return DateStarted.Value.Year;
                return 0;
            }
        }

        [JsonIgnore]
        public string SeasonText
        {
            get
            {
                return Year + " " + Season;
            }
        }

        public bool ReferenceMatchable()
        {
            if (DateStarted == null) return false;
            if (Type == AnimeType.Unknown) return false;

            return true;
        }

        public bool Match(string query)
        {
            query = StringHelper.Simplify(query);
            double percentage = 0.45;

            if (StringHelper.LevenshteinPercentage(query, StringHelper.Simplify(Title)) >= percentage) return true;

            if (TitleEnglish != null)
            {
                if (StringHelper.LevenshteinPercentage(query, StringHelper.Simplify(TitleEnglish)) >= percentage) return true;
            }

            if (TitleJapanese != null)
            {
                if (StringHelper.LevenshteinPercentage(query, StringHelper.Simplify(TitleJapanese)) >= percentage) return true;
            }

            foreach (Synonym synonym in Synonyms)
            {
                if (string.IsNullOrEmpty(synonym.Name)) continue;
                if (StringHelper.LevenshteinPercentage(query, StringHelper.Simplify(synonym.Name)) >= percentage) return true;
            }

            return false;
        }

        public void Merge(Anime other)
        {
            if (Api != other.Api) throw new Exception("Cannot merge anime of different API");
            if (ServerId != other.ServerId) throw new Exception("Cannot merge anime of different ServerId");

            if (!string.IsNullOrEmpty(other.Slug)) Slug = other.Slug;
            if (!string.IsNullOrEmpty(other.Synopsis)) Synopsis = other.Synopsis;
            if (other.Type != AnimeType.Unknown) Type = other.Type;
            if (other.Rank != null) Rank = other.Rank;
            if (other.PopularityRank != null) PopularityRank = other.PopularityRank;
            if (other.ImageUrl != null) ImageUrl = other.ImageUrl;
            if (other.Episodes != null) Episodes = other.Episodes;
            if (other.EpisodeLength != null) EpisodeLength = other.EpisodeLength;
            if (other.AirStatus != AirStatus.Unknown) AirStatus = other.AirStatus;
            if (!string.IsNullOrEmpty(other.Title)) Title = other.Title;
            if (!string.IsNullOrEmpty(other.TitleEnglish)) TitleEnglish = other.TitleEnglish;
            if (!string.IsNullOrEmpty(other.TitleJapanese)) TitleJapanese = other.TitleJapanese;
            if (!string.IsNullOrEmpty(other.Classification)) Classification = other.Classification;
            if (other.MembersScore != null) MembersScore = other.MembersScore;
            if (other.MembersCount != null) MembersCount = other.MembersCount;
            if (other.FavoritedCount != null) FavoritedCount = other.FavoritedCount;
            if (other.DateStarted != null) DateStarted = other.DateStarted;
            if (other.DateEnded != null) DateEnded = other.DateEnded;

            //if (other.Genres.Count > 0) Genres = other.Genres;
            //if (other.Synonyms.Count > 0) Synonyms = other.Synonyms;
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1} ({2})", Api.ToString(), Title, ServerId);
        }
    }

    public static class AnimeExtensions
    {
        public static IQueryable<Anime> IncludeDetails(this IQueryable<Anime> query)
        {
            return query
                .Include(a => a.Genres)
                .Include(a => a.Synonyms)
                .Include(a => a.Reference);
        }
    }
}
