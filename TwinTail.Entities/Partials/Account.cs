﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace TwinTail.Entities
{
    public partial class Account
    {
        public override string ToString()
        {
            return "[" + Api + "] " + Username;
        }
    }
}
