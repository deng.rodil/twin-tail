﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;

using Sylpheed.Helpers;

namespace TwinTail.Entities
{
    public partial class ArchiveEntry
    {
        public static ArchiveEntry CreateDefault()
        {
            ArchiveEntry entry = new ArchiveEntry();
            entry.Status = ArchiveStatus.Pending;
            entry.Source = VideoSource.TV;
            entry.VideoWidth = 1280;
            entry.VideoHeight = 720;

            return entry;
        }

        public override string ToString()
        {
            return Anime.Title + "[" + Source + "] [" + VideoHeight + "p]";
        }
    }

    public static class ArchiveEntryExtension
    {
        public static IQueryable<ArchiveEntry> IncludeDetails(this IQueryable<ArchiveEntry> query)
        {
            return query
                .Include(a => a.Account)
                .Include(a => a.Anime)
                .Include(a => a.Fansubs)
                .Include(a => a.Anime.Genres)
                .Include(a => a.Anime.Synonyms);
        }
    }
}
