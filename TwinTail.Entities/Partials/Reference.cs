﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinTail.Entities
{
    public partial class Reference
    {
        public Anime GetAnime(string api)
        {
            return Animes.SingleOrDefault(a => a.Api == api);
        }

        public bool HasAnime(Anime anime)
        {
            return Animes.Any(a => a.Id == anime.Id);
        }

        public IEnumerable<string> MissingReferences
        {
            get
            {
                using (var db = new TwinTailDb())
                {
                    var query = from api in db.ApiServices
                                where api.Enabled
                                where Animes.Any(a => a.Api == api.Type)
                                select api.Type;

                    return query.ToList();
                }
            }
        }

        public bool HasMissingReference()
        {
            return MissingReferences.Any();
        }

        public void IncludeAnime(Anime anime)
        {
            Anime existing = Animes.SingleOrDefault(a => a.Api == anime.Api);

            if (existing != null)
            {
                Animes.Remove(existing);
            }

            Animes.Add(anime);
        }
    }
}
