﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinTail.Entities
{
    public partial class Synonym
    {
        public Synonym()
        {
        }

        public Synonym(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
