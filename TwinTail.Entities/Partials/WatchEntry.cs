﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;

namespace TwinTail.Entities
{
    public partial class WatchEntry
    {
        public override string ToString()
        {
            return Anime.ToString();
        }

        [JsonIgnore]
        public float EpisodeProgress
        {
            get
            {
                // Unknown episode
                if (Anime.Episodes <= 0)
                {
                    int predictedEnd = Episode + 13 - (Episode % 13);
                    return Episode / predictedEnd;
                }

                return Episode / (float)Anime.Episodes.Value;
            }
        }

        [JsonIgnore]
        public string EpisodeProgressText
        {
            get
            {
                string end;
                if (Anime.Episodes <= 0) end = "?";
                else end = Anime.Episodes.ToString();

                return Episode.ToString() + "/ " + end;
            }
        }
    }

    public static class WatchEntryExtensions
    {
        public static IQueryable<WatchEntry> IncludeDetails(this IQueryable<WatchEntry> query)
        {
            return query
                .Include(w => w.Account)
                .Include(w => w.Anime)
                .Include(w => w.Fansubs);
        }
    }
}
