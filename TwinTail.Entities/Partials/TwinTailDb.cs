﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinTail.Entities
{
    public partial class TwinTailDb
    {
        public void EmptyDatabase()
        {
            Database.Delete();
            Database.CreateIfNotExists();
        }

        public bool Exists<T>(T entity) where T : class
        {
            return this.Set<T>().Local.Any(e => e == entity);
        }
    }
}
