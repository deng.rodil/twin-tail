﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using TwinTail.Services;
using TwinTail.Entities;
using Microsoft.Practices.Unity;

namespace TwinTail.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IUnityContainer container;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ConfigureDependencies();

            MainWindow mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();
        }

        private void ConfigureDependencies()
        {
            container = new UnityContainer();
            container.RegisterInstance(container, new ContainerControlledLifetimeManager());
            container.RegisterType<IAnimeCoverImageLoader, AnimeCoverImageLoader>();
            
            container.RegisterType<IApiService, ApiManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<IAccountService, AccountManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<IWatchService, WatchLibrary>(new ContainerControlledLifetimeManager());

            container.RegisterType<TwinTailDb>(new HierarchicalLifetimeManager());
            container.RegisterType<IAnimeLibraryService, AnimeLibrary>(new HierarchicalLifetimeManager());
            container.RegisterType<IArchiveService, ArchiveLibrary>(new HierarchicalLifetimeManager());
            container.RegisterType<IFansubService, FansubLibrary>(new HierarchicalLifetimeManager());
        }
    }
}
