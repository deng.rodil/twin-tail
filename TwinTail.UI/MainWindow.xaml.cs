﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

using TwinTail.Entities;
using TwinTail.Services;
using TwinTail.UI.Views;
using TwinTail.UI.ViewModels;
using Microsoft.Practices.Unity;

namespace TwinTail.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IUnityContainer container;
        private readonly IUnityContainer childContainer;
        private readonly IAccountService accountService;
        private readonly IWatchService watchService;
        private readonly IApiService apiService;
        
        private List<Anime> animeList = new List<Anime>();

        public MainWindow(IUnityContainer container, IAccountService accountService, IApiService apiService, IWatchService watchService)
        {
            InitializeComponent();

            this.container = container;
            childContainer = container.CreateChildContainer();
            this.accountService = accountService;
            this.apiService = apiService;
            this.watchService = watchService;

            childContainer.BuildUp<WatchListControl>(watchListControl);
            childContainer.BuildUp<ArchiveListControl>(archiveListControl);
        }

        async Task Login()
        {
            statusBarMessage.Content = "Logging in to " + apiService.DefaultClient.Id + "...";
            bool authenticated = await accountService.Authenticate("MyAnimeList");
            if (!authenticated)
            {
                await accountService.SetupAccount("MyAnimeList", "Sylpheed", "iseria1769");
            }
        }

        void InitializeDatabase()
        {
            using (var db = new TwinTailDb())
            {
                bool created = db.Database.CreateIfNotExists();
                if (created) Console.WriteLine("Database created");
                else Console.WriteLine("Found existing database");
            }
        }

        void ClearData()
        {
            using (var db = new TwinTailDb())
            {
                db.EmptyDatabase();
            }

            animeList.Clear();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeDatabase();

            await Login();
            statusBarMessage.Content = "";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            childContainer.Dispose();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("This will clear all data in local database. Do you really want to clear everything?", "Clear Data", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes) ClearData();
        }
    }
}
