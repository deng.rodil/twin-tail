﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PropertyChanged;
using Microsoft.Practices.Unity;
using TwinTail.Entities;
using TwinTail.Services;
using System.Collections.ObjectModel;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class WatchListViewModel
    {
        [Dependency]
        public IWatchService WatchService { get; set; }
        [Dependency]
        public IAccountService AccountService { get; set; }

        public ObservableCollection<WatchEntry> Entries { get; set; }

        public async Task ReloadFromLocal(bool cached)
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) return;

            Entries = new ObservableCollection<WatchEntry>(await Task.Run(() => WatchService.GetLocalLibrary(account, cached)));
        }

        public async Task ReloadFromServer()
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) return;

            Entries = new ObservableCollection<WatchEntry>(await Task.Run(() => WatchService.GetRemoteLibrary(account)));
        }
    }
}
