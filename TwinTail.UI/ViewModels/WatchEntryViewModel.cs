﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;
using TwinTail.Services;
using PropertyChanged;
using Microsoft.Practices.Unity;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class WatchEntryViewModel
    {
        [Dependency]
        public IWatchService WatchService { get; set; }
        [Dependency]
        public IAccountService AccountService { get; set; }

        public WatchEntry WatchEntry { get; set; }

        public async Task LoadFromAnime(Anime anime)
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            WatchEntry = await WatchService.GetEntry(account, anime);
        }

        public bool Complete()
        {
            if (WatchEntry.Status == WatchStatus.Completed) return false;

            if (WatchEntry.Anime.Episodes != null) WatchEntry.Episode = WatchEntry.Anime.Episodes.Value;
            WatchEntry.DateEnded = DateTime.Today;
            WatchEntry.Episode = WatchEntry.Anime.Episodes.Value;

            return true;
        }

        public async Task SaveChanges()
        {
            await WatchService.UpdateEntry(WatchEntry);
            await Task.Delay(0);
        }

        public async Task RevertChanges()
        {
            await WatchService.ReloadEntry(WatchEntry);
            //await Task.Run(() => WatchEntry.RevertChanges());
        }
    }
}
