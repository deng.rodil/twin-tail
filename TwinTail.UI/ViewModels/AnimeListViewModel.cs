﻿using Microsoft.Practices.Unity;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinTail.Entities;
using TwinTail.Services;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class AnimeListViewModel
    {
        [Dependency]
        public IAnimeLibraryService AnimeLibraryService { get; set; }
        [Dependency]
        public IWatchService WatchService { get; set; }

        public ObservableCollection<Anime> Entries { get; set; }

        public async Task Reload()
        {
            Entries = new ObservableCollection<Anime>(await Task.Run(() => AnimeLibraryService.GetUserLibrary("MyAnimeList")));
            //IEnumerable<WatchEntry> watchEntries = await Task.Run(() => WatchService.GetServerLibrary("MyAnimeList"));
            //Entries = new ObservableCollection<Anime>(watchEntries.Select(w => w.Anime));
        }

        public async Task ReloadAnime(Anime anime)
        {
            await AnimeLibraryService.ReloadAnime(anime);
        }
    }
}
