﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using PropertyChanged;
using Microsoft.Practices.Unity;
using TwinTail.Entities;
using TwinTail.Services;
using System.IO;
using Microsoft.Win32;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class ArchiveListViewModel
    {
        [Dependency]
        public IArchiveService ArchiveService { get; set; }
        [Dependency]
        public IAccountService AccountService { get; set; }

        public ObservableCollection<ArchiveEntry> Entries { get; set; }

        public async Task Reload()
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) return;

            Entries = new ObservableCollection<ArchiveEntry>(await Task.Run(() => ArchiveService.GetLibrary(account)));
        }

        public async Task Import()
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) return;

            // Open file dialog
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = ".json";
            fileDialog.Filter = "JSON Files (*.json)|*.json";
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\TwinTail\\archive\\" + account.Api + "\\";
            var dlgResult = fileDialog.ShowDialog();
            if (dlgResult == false) return;

            // Get file path
            FileInfo file = new FileInfo(fileDialog.FileName);

            // Load file
            string json = null;
            using (var stream = file.OpenText())
            {
                json = await stream.ReadToEndAsync();
            }

            await Task.Run(() => ArchiveService.ImportEntries(json));
            await Reload();
        }

        public async Task Export()
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) return;

            await Task.Run(() => ArchiveService.ExportEntries(account));
        }
    }
}
