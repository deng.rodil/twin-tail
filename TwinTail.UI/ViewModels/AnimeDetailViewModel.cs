﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PropertyChanged;
using TwinTail.Entities;
using TwinTail.Services;
using Microsoft.Practices.Unity;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class AnimeDetailViewModel
    {
        [Dependency]
        public IAnimeLibraryService AnimeLibraryService { get; set; }
        [Dependency]
        public IAnimeCoverImageLoader ImageLoader { get; set; }

        public Anime Anime { get; set; }

        public string Synonyms
        {
            get
            {
                if (Anime == null) return "Loading...";
                return String.Join(", ", Anime.Synonyms.Select(g => g.Name).ToArray());
            }
        }

        public string Genres
        {
            get
            {
                if (Anime == null) return "Loading...";
                return String.Join(", ", Anime.Genres.Select(g => g.Name).ToArray());
            }
        }

        public string MemberScore
        {
            get
            {
                if (Anime == null) return "Loading...";
                if (Anime.MembersScore == null) return "?";
                return Anime.MembersScore.ToString();
            }
        }

        public string YearSeason
        {
            get
            {
                if (Anime == null) return "Loading...";
                return Anime.Year + " " + Anime.Season;
            }
        }

        public string CoverImagePath { get; protected set; }
        public BitmapImage CoverImage { get; protected set; }

        public async Task ResyncAnime()
        {
            // Get local
            Anime = await AnimeLibraryService.GetAnime(Anime.ServerId, Anime.Api);
            CoverImage = ImageLoader.LoadLocalImage(Anime);

            // Get remote
            Anime fresh = await AnimeLibraryService.ResyncAnime(Anime);
            CoverImage = await ImageLoader.LoadRemoteImage(fresh);

            Anime = null;
            Anime = fresh;
        }
    }
}
