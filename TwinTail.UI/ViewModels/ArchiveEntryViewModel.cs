﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using PropertyChanged;
using Microsoft.Practices.Unity;
using TwinTail.Entities;
using TwinTail.Services;
using Sylpheed.Helpers;
using Mvvm;

namespace TwinTail.UI.ViewModels
{
    [ImplementPropertyChanged]
    public class ArchiveEntryViewModel
    {
        [Dependency]
        public IFansubService FansubService { get; set; }
        [Dependency]
        public IAccountService AccountService { get; set; }
        [Dependency]
        public IArchiveService ArchiveService { get; set; }
        [Dependency]
        public IAnimeLibraryService AnimeService { get; set; }

        public Anime Anime { get; protected set; }
        public ArchiveEntry ArchiveEntry { get; protected set; }
        public bool HasArchive { get { return ArchiveEntry != null; } }
        public ObservableCollection<Fansub> AvailableFansubs { get; set; } = new ObservableCollection<Fansub>();
        public string AddOrDeleteText { get { return HasArchive ? "Delete" : "Add"; } }
        public ObservableCollection<Fansub> Fansubs { get; protected set; } = new ObservableCollection<Fansub>();

        public async Task LoadFromAnime(Anime anime)
        {
            Account account = AccountService.GetAccount("MyAnimeList");
            if (account == null) throw new Exception("No account logged in");

            // Load entity from this context
            Anime = await AnimeService.GetAnime(anime.ServerId, anime.Api);
            ArchiveEntry = await ArchiveService.GetEntry(account, Anime);
        }

        public async Task RefreshFansubs()
        {
            IEnumerable<Fansub> fansubs = await FansubService.GetAll();

            if (ArchiveEntry != null)
            {
                fansubs = fansubs.Except(ArchiveEntry.Fansubs, f => f.Id);
                Fansubs = new ObservableCollection<Fansub>(ArchiveEntry.Fansubs);
            }

            AvailableFansubs = new ObservableCollection<Fansub>(fansubs);
        }

        public async Task AddFansub(string name, Action onDuplicate, Func<bool> onFansubCreate)
        {
            // Check if this fansub already exists in the archive entry
            bool exists = ArchiveEntry.Fansubs.Where(f => f.ShortName == name).Any();
            if (exists)
            {
                onDuplicate();
                return;
            }

            // Get fansub from database
            Fansub fansub = await FansubService.Search(f => f.ShortName == name);

            // Create fansub if it doesn't exist yet
            if (fansub == null)
            {
                if (!onFansubCreate()) return;
                fansub = new Fansub { ShortName = name };
            }

            // Add fansub to archive
            ArchiveEntry.Fansubs.Add(fansub);

            await RefreshFansubs();
        }

        public async Task RemoveFansub(Fansub fansub)
        {
            if (!HasArchive) throw new Exception("Cannot remove fansub for null ArchiveEntry");

            // Remove fansub
            ArchiveEntry.Fansubs.Remove(fansub);

            await RefreshFansubs();
        }

        public async Task AddOrDeleteArchive(Func<bool> willCreateArchive, Func<bool> willDeleteArchive)
        {
            // Create new entry if it's still null
            if (!HasArchive)
            {
                if (!willCreateArchive()) return;

                ArchiveEntry = ArchiveEntry.CreateDefault();
                ArchiveEntry.Anime = Anime;
                ArchiveEntry.Account = AccountService.GetAccount("MyAnimeList");

                await RefreshFansubs();
            }
            else
            {
                if (!willDeleteArchive()) return;

                // Delete
                await ArchiveService.DeleteEntry(ArchiveEntry);
                ArchiveEntry = null;
            }
        }

        public async Task SaveChanges()
        {
            if (!HasArchive) return;
            await ArchiveService.SaveEntry(ArchiveEntry);
        }

        public async Task RevertChanges()
        {
            if (!HasArchive) return;
            await ArchiveService.ReloadEntry(ArchiveEntry);
        }
    }
}
