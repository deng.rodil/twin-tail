﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwinTail.Entities;
using TwinTail.UI.Helpers;
using TwinTail.UI.ViewModels;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for WatchListControl.xaml
    /// </summary>
    public partial class WatchListControl : UserControl
    {
        private bool loadedOnce = false;

        [Dependency]
        public WatchListViewModel ViewModel
        {
            get { return DataContext as WatchListViewModel; }
            set { DataContext = value; }
        }

        [Dependency]
        public IUnityContainer Container { get; set; }

        public WatchListControl()
        {
            InitializeComponent();
        }

        private async void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = WpfHelper.ExtractDataGridRow(e.OriginalSource as DependencyObject);
            if (row == null) return;

            WatchEntry selected = row.Item as WatchEntry;

            using (var container = Container.CreateChildContainer())
            {
                AnimeEditWindow detail = container.Resolve<AnimeEditWindow>();
                await detail.LoadAnime(selected.Anime);
                detail.ShowDialog();
            }
        }

        private async void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (IsVisible) await ViewModel.ReloadFromLocal(true);
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (loadedOnce) return;
            loadedOnce = true;

            btnRefetch.IsEnabled = false;
            btnRefetch.Content = "Syncing...";

            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (IsVisible) await ViewModel.ReloadFromServer();

            btnRefetch.IsEnabled = true;
            btnRefetch.Content = "Resync";
        }

        private async void btnRefetch_Click(object sender, RoutedEventArgs e)
        {
            btnRefetch.IsEnabled = false;
            btnRefetch.Content = "Syncing...";

            await ViewModel.ReloadFromServer();

            btnRefetch.IsEnabled = true;
            btnRefetch.Content = "Resync";
        }
    }
}
