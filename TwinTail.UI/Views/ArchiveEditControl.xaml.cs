﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using TwinTail.Entities;
using TwinTail.Services;
using TwinTail.UI.ViewModels;
using Microsoft.Practices.Unity;
using Sylpheed.Helpers;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for AnimeArchiveControl.xaml
    /// </summary>
    public partial class ArchiveEditControl : UserControl
    {
        [Dependency]
        public ArchiveEntryViewModel ViewModel
        {
            get { return DataContext as ArchiveEntryViewModel; }
            set { DataContext = value; }
        }

        public ArchiveEditControl()
        {
            InitializeComponent();
        }

        private async Task AddFansub(string name)
        {
            await ViewModel.AddFansub(name, delegate
            {
                MessageBox.Show(name + " is already added to " + ViewModel.Anime.Title, "Duplicate fansub entry");
            }, delegate
            {
                MessageBoxResult result = MessageBox.Show("Do you want to create a new fansub entry for " + name + "?", "Create new fansub", MessageBoxButton.YesNo);
                return result == MessageBoxResult.Yes;
            });

        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            // Initialize ArchiveStatus values
            comboArchiveStatus.ItemsSource = Enum.GetValues(typeof(ArchiveStatus));

            // Initialize ArchiveSource values
            comboArchiveSource.ItemsSource = Enum.GetValues(typeof(VideoSource));

            await ViewModel.RefreshFansubs();
        }

        private async void btnArchiveAddOrUpdate_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.AddOrDeleteArchive(delegate
            {
                MessageBoxResult result = MessageBox.Show("Do you want to create an archive entry for " + ViewModel.Anime.Title + "?", "Create archive entry", MessageBoxButton.YesNo);
                return result == MessageBoxResult.Yes;
            }, delegate
            {
                MessageBoxResult result = MessageBox.Show("Do you want to create an archive entry for " + ViewModel.Anime.Title + "?", "Create archive entry", MessageBoxButton.YesNo);
                return result == MessageBoxResult.Yes;
            });

        }

        private async void listArchiveFansub_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = sender as ListBox;
            if (box.SelectedItem == null) return;

            Fansub fansub = box.SelectedItem as Fansub;

            MessageBoxResult result = MessageBox.Show("Do you want to remove " + fansub.ShortName + " from the archived anime?", "Delete fansub group", MessageBoxButton.YesNo);
            if (result != MessageBoxResult.Yes) return;

            await ViewModel.RemoveFansub(fansub);
        }

        private async void btnAddFansub_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(comboAddFansub.Text)) return;

            await AddFansub(comboAddFansub.Text);
        }

        private async void comboAddFansub_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            if (e.Key == Key.Enter)
            {
                await AddFansub(box.Text);
            }
        }
    }
}
