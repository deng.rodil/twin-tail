﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using TwinTail.UI.ViewModels;
using Microsoft.Practices.Unity;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for AnimeDetailControl.xaml
    /// </summary>
    public partial class AnimeDetailControl : UserControl
    {
        [Dependency]
        public AnimeDetailViewModel ViewModel
        {
            get { return DataContext as AnimeDetailViewModel; }
            set { DataContext = value; }
        }

        public AnimeDetailControl()
        {
            InitializeComponent();
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            await ViewModel.ResyncAnime();
        }
    }
}
