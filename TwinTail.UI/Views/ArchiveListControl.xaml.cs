﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using TwinTail.UI.ViewModels;
using TwinTail.Services;
using TwinTail.Entities;
using TwinTail.UI.Helpers;

using Microsoft.Practices.Unity;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for ArchiveListControl.xaml
    /// </summary>
    public partial class ArchiveListControl : UserControl
    {
        [Dependency]
        public ArchiveListViewModel ViewModel
        {
            get { return DataContext as ArchiveListViewModel; }
            set { DataContext = value; }
        }

        [Dependency]
        public IUnityContainer Container { get; set; }

        public ArchiveListControl()
        {
            InitializeComponent();
        }

        private async void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if ((bool)e.NewValue) await ViewModel.Reload();
        }

        private async void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = WpfHelper.ExtractDataGridRow(e.OriginalSource as DependencyObject);
            if (row == null) return;

            ArchiveEntry selected = row.Item as ArchiveEntry;

            using (var container = Container.CreateChildContainer())
            {
                AnimeEditWindow detail = container.Resolve<AnimeEditWindow>();
                await detail.LoadAnime(selected.Anime);
                detail.ShowDialog();
            }
        }

        private async void btnImport_Click(object sender, RoutedEventArgs e)
        {
            btnImport.Content = "Importing...";
            btnImport.IsEnabled = false;
            btnExport.IsEnabled = false;

            await ViewModel.Import();

            btnImport.Content = "Import";
            btnImport.IsEnabled = true;
            btnExport.IsEnabled = true;
        }

        private async void btnExport_Click(object sender, RoutedEventArgs e)
        {
            btnExport.Content = "Exporting...";
            btnExport.IsEnabled = false;
            btnImport.IsEnabled = false;

            await ViewModel.Export();

            btnExport.Content = "Export";
            btnExport.IsEnabled = true;
            btnImport.IsEnabled = true;
        }
    }
}
