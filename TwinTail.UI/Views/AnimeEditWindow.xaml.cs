﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using TwinTail.Entities;
using TwinTail.Services;
using TwinTail.UI.ViewModels;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for WatchEntryDetailWindow.xaml
    /// </summary>
    public partial class AnimeEditWindow : Window
    {
        public Anime Anime { get; private set; }

        private IUnityContainer animeDetailContainer;
        private IUnityContainer archiveEditContainer;
        private IUnityContainer watchEditContainer;

        public AnimeEditWindow(IUnityContainer container)
        {
            InitializeComponent();

            animeDetailContainer = container.CreateChildContainer();
            archiveEditContainer = container.CreateChildContainer();
            watchEditContainer = container.CreateChildContainer();

            animeDetailContainer.BuildUp<AnimeDetailControl>(animeDetailControl);
            archiveEditContainer.BuildUp<ArchiveEditControl>(archiveControl);
            watchEditContainer.BuildUp<WatchEditControl>(watchEditControl);
        }

        public async Task LoadAnime(Anime anime)
        {
            Anime = anime;
            DataContext = anime;

            animeDetailControl.ViewModel.Anime = anime;
            await archiveControl.ViewModel.LoadFromAnime(anime);
            await watchEditControl.ViewModel.LoadFromAnime(anime);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            await archiveControl.ViewModel.SaveChanges();
            //await watchEditControl.ViewModel.SaveChanges();
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            animeDetailContainer.Dispose();
            archiveEditContainer.Dispose();
            watchEditContainer.Dispose();
        }
    }
}
