﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TwinTail.UI.ViewModels;
using TwinTail.Entities;
using Microsoft.Practices.Unity;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for WatchEditControl.xaml
    /// </summary>
    public partial class WatchEditControl : UserControl
    {
        [Dependency]
        public WatchEntryViewModel ViewModel
        {
            get { return DataContext as WatchEntryViewModel; }
            set { DataContext = value; }
        }

        public WatchEditControl()
        {
            InitializeComponent();
        }

        private void comboWatchStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (box.SelectedItem == null) return;
            WatchStatus status = (WatchStatus)box.SelectedItem;

            if (ViewModel.Complete())
            {
                dateWatchCompleted.DisplayDate = ViewModel.WatchEntry.DateEnded.Value;
                dateWatchCompleted.SelectedDate = ViewModel.WatchEntry.DateEnded.Value;
                numEpisodes.Value = ViewModel.WatchEntry.Episode;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            comboWatchStatus.ItemsSource = Enum.GetValues(typeof(WatchStatus));
        }
    }
}
