﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using TwinTail.Entities;
using TwinTail.UI.Helpers;
using TwinTail.Services;
using TwinTail.UI.ViewModels;
using Microsoft.Practices.Unity;

namespace TwinTail.UI.Views
{
    /// <summary>
    /// Interaction logic for AnimeListView.xaml
    /// </summary>
    public partial class AnimeListControl : UserControl
    {
        [Dependency]
        public AnimeListViewModel ViewModel
        {
            get { return DataContext as AnimeListViewModel; }
            set { DataContext = value; }
        }

        [Dependency]
        public IUnityContainer Container { get; set; }

        public AnimeListControl()
        {
            InitializeComponent();
        }

        private async void animeList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = WpfHelper.ExtractDataGridRow(e.OriginalSource as DependencyObject);
            if (row == null) return;

            Anime selectedAnime = row.Item as Anime;

            using (var container = Container.CreateChildContainer())
            {
                AnimeEditWindow detail = container.Resolve<AnimeEditWindow>();
                await detail.LoadAnime(selectedAnime);
                detail.ShowDialog();

                // Reload anime
                await ViewModel.ReloadAnime(selectedAnime);
                row.Item = null;
                row.Item = selectedAnime;
            }
        }

        private async void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (IsVisible) await ViewModel.Reload();
        }
    }
}
