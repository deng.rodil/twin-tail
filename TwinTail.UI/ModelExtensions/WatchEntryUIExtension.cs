﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinTail.Entities;

namespace TwinTail.UI.ModelExtensions
{
    public partial class WatchEntry
    {
        private List<WatchStatus> statusList;
        public IList<WatchStatus> StatusList
        {
            get
            {
                if (statusList == null)
                {
                    statusList = new List<WatchStatus>();
                }

                return statusList;
            }
        }
    }
}
