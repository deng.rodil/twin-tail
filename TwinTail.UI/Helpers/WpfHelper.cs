﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace TwinTail.UI.Helpers
{
    public static class WpfHelper
    {
        public static DataGridRow ExtractDataGridRow(DependencyObject dep)
        {
            DataGridCell cell = ExtractDataGridCell(dep);
            if (cell == null) return null;

            // navigate further up the tree
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            return dep as DataGridRow;
        }

        public static DataGridCell ExtractDataGridCell(DependencyObject dep)
        {
            // iteratively traverse the visual tree
            while ((dep != null) && !(dep is DataGridCell))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null) return null;
            return dep as DataGridCell;
        }
    }
}
