﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtheruneAnimeArchive.Classes.API.Plugins.MVVM
{
    public static class ObservableRangeCollection
    {
        public static void AddRange<T>(this ObservableCollection<T> myCollection, IEnumerable<T> collection)
        {
            if (collection == null) throw new ArgumentNullException("collection");

            foreach (var i in collection) myCollection.Add(i);
            //myCollection.Co
            //OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, collection.ToList()));
        }
    }
}
