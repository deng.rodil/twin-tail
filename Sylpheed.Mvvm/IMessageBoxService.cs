﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Sylpheed.Mvvm
{
    public enum MessageBoxResponse
    {
        None = 0,
        OK = 1,
        Cancel = 2,
        Yes = 6,
        No = 7
    }

    public interface IMessageBoxService
    {
        MessageBoxResponse Show();
    }
}
