﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sylpheed.Mvvm
{
    public interface IWindowService
    {
        void OpenWindow(object viewModel);
        void OpenDialog(object viewModel);
    }
}
