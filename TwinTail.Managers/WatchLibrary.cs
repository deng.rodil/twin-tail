﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TwinTail.Entities;

using Microsoft.Practices.Unity;
using Sylpheed.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TwinTail.Services
{
    public class WatchLibrary : GenericRepository<WatchEntry, TwinTailDb>, IWatchService
    {
        private IApiService apiService;
        private string cacheDirPath;

        private List<WatchEntry> cachedEntries = new List<WatchEntry>();

        public WatchLibrary(TwinTailDb context, IApiService apiService)
            : base(context)
        {
            this.apiService = apiService;

            // Setup file path
            cacheDirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\TwinTail\\watch\\";
        }

        public async Task<IEnumerable<WatchEntry>> GetLocalLibrary(Account account, bool retrieveCache = false)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            if (retrieveCache && cachedEntries.Count > 0) return cachedEntries;

            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + account.Api + "\\");
            if (!dir.Exists) return cachedEntries;

            FileInfo file = new FileInfo(dir.FullName + "watch-" + account.Username + ".json");
            if (!file.Exists) return cachedEntries;

            string s;
            using (var stream = file.OpenText())
            {
                s = await stream.ReadToEndAsync();
            }

            // Convert json
            cachedEntries = JsonConvert.DeserializeObject<List<WatchEntry>>(s).OrderBy(w => w.Anime.Title).ToList();
            return cachedEntries;
        }

        public async Task<IEnumerable<WatchEntry>> GetRemoteLibrary(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            var watchEntries = await apiService.GetApiClient(account.Api).GetWatchEntries(account.Username);
            watchEntries = watchEntries.OrderBy(w => w.Anime.Title).ToList();

            // Cache new anime entries
            await CacheNewAnimeEntries(account.Api, watchEntries.Select(w => w.Anime));

            // Cache the fresh watch entries
            await CacheWatchEntries(account, watchEntries);

            return watchEntries;
        }

        private async Task CacheWatchEntries(Account account, IEnumerable<WatchEntry> entries)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            // Convert to json
#warning - Consider writing a serializer. Won't work if the TT files are regenerated
            JToken json = JArray.FromObject(entries);

            // Setup IO
            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + account.Api + "\\");
            if (!dir.Exists) dir.Create();

            FileInfo file = new FileInfo(dir.FullName + "watch-" + account.Username + ".json");
            if (file.Exists) file.Delete();
            using (var writer = file.CreateText())
            {
                await writer.WriteAsync(json.ToString());
            }

            cachedEntries.Clear();
            cachedEntries.AddRange(entries);
        }

        private async Task CacheNewAnimeEntries(string api, IEnumerable<Anime> entries)
        {
            // Stop on empty entries
            if (!entries.Any()) return;

            // Determine new entries based on existing entries
            var existingEntries = await context.Animes.Where(a => a.Api == api).ToListAsync();
            var newEntries = entries.Except(existingEntries, a => a.ServerId);

            // Save new entries
            foreach (Anime anime in newEntries)
            {
                Console.WriteLine("Caching " + anime + "...");
                context.Animes.Add(anime);
            }
            await context.SaveChangesAsync();
        }

        public async Task<WatchEntry> GetEntry(Account account, Anime anime)
        {
            // Load cache if not yet loaded
            if (cachedEntries.Count == 0) await GetLocalLibrary(account);
            return cachedEntries.Where(w => w.Anime.ServerId == anime.ServerId).SingleOrDefault();
        }

        public async Task UpdateEntry(WatchEntry entry)
        {
            // Update server
            bool success = await apiService.GetApiClient(entry.Anime.Api).UpdateAnime(entry);
            if (!success)
            {
                Console.WriteLine("Failed to update " + entry);
                return;
            }

            // TODO: Update memory cache
#warning TODO: Update memory cache
        }

        public async Task DeleteEntry(WatchEntry entry)
        {
            // Update server
            bool success = await apiService.GetApiClient(entry.Anime.Api).DeleteAnime(entry.Anime.ServerId);
            if (!success) return;

            // Update local db
            Delete(entry);
            await context.SaveChangesAsync();
        }

        public async Task ReloadEntry(WatchEntry entry)
        {
            await Revert(entry);
        }
    }
}
