﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IAnimeCoverImageLoader
    {
        /// <summary>
        /// Loads image from Anime.ImageUrl
        /// </summary>
        /// <param name="anime"></param>
        /// <returns></returns>
        Task<BitmapImage> LoadRemoteImage(Anime anime);
        /// <summary>
        /// Loads image from hard disk.
        /// </summary>
        /// <param name="anime"></param>
        /// <returns>Returns null if it wasn't previously cached</returns>
        BitmapImage LoadLocalImage(Anime anime);
        /// <summary>
        /// Tries to load image from local cache. If it doesn't exist, load image from Anime.ImageUrl
        /// </summary>
        /// <param name="anime"></param>
        /// <returns></returns>
        Task<BitmapImage> LoadImage(Anime anime);
    }
}
