﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IAnimeLibraryService
    {
        Task<IEnumerable<Anime>> GetLibrary(string api);
        Task<IEnumerable<Anime>> GetUserLibrary(string api);
        Task<Anime> ResyncAnime(Anime anime);
        Task<Anime> GetAnime(string animeId, string api);
        Task<IEnumerable<Anime>> SearchAnime(string query, string api = null);
        Task SaveAnime(Anime anime);
        Task ReloadAnime(Anime anime);
    }
}
