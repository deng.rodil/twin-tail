﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IAccountService
    {
        Task LoadAccounts();
        Task<bool> Authenticate(string api);
        Task<bool> SetupAccount(string api, string username, string password);
        bool IsAuthenticated(string api);
        Account GetAccount(string api);

        IEnumerable<Account> Accounts { get; }
    }
}
