﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IArchiveService
    {
        Task<IEnumerable<ArchiveEntry>> GetLibrary(Account account);
        Task<ArchiveEntry> GetEntry(Account account, Anime anime);
        Task<ArchiveEntry> SearchEntry(Account account, string searchKey);
        Task SaveEntry(ArchiveEntry entry);
        Task ReloadEntry(ArchiveEntry entry);
        Task DeleteEntry(ArchiveEntry entry);

        Task<string> ExportEntries(Account account);
        Task ImportEntries(string json);
    }
}
