﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IWatchService
    {
        Task<IEnumerable<WatchEntry>> GetLocalLibrary(Account account, bool retrieveCache = false);
        Task<IEnumerable<WatchEntry>> GetRemoteLibrary(Account account);
        Task<WatchEntry> GetEntry(Account account, Anime anime);
        Task UpdateEntry(WatchEntry entry);
        Task DeleteEntry(WatchEntry entry);
        Task ReloadEntry(WatchEntry entry);
    }
}
