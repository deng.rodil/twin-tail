﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Api;

namespace TwinTail.Services
{
    public interface IApiService
    {
        IEnumerable<ApiClient> Clients { get; }
        ApiClient DefaultClient { get; }
        ApiClient GetApiClient(string api);
        void RegisterApi(ApiClient api, bool setDefault = false);
        void ResignApi(string api);
    }

    #region Exceptions
    public class ApiClientNotRegisteredException : Exception
    {
        public ApiClientNotRegisteredException(string api)
            : base(string.Format("{0} is not registered", api.ToString()))
        {
        }
    }
    #endregion
}
