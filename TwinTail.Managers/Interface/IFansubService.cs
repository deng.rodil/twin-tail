﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public interface IFansubService
    {
        Task<IEnumerable<Fansub>> GetAll();
        Task<Fansub> Search(Expression<Func<Fansub, bool>> predicate);
        Task<IEnumerable<Fansub>> SearchMany(Expression<Func<Fansub, bool>> predicate);
    }
}
