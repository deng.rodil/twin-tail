﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinTail.Services
{
    public interface IFactory
    {
        T Resolve<T>();
        T BuildUp<T>(T obj);
    }
}
