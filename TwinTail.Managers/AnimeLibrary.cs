﻿using Sylpheed.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using TwinTail.Entities;
using TwinTail.Api;

namespace TwinTail.Services
{
    public class AnimeLibrary : GenericRepository<Anime, TwinTailDb>, IAnimeLibraryService
    {
        private IApiService apiService;

        public AnimeLibrary(TwinTailDb context, IApiService apiService)
            : base(context)
        {
            this.apiService = apiService;
        }

        public async Task<IEnumerable<Anime>> GetLibrary(string api)
        {
            var query = context.Animes.Where(a => a.Api == api).OrderBy(a => a.Title);
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<Anime>> GetUserLibrary(string api)
        {
            List<Anime> userEntries = new List<Anime>();

            // Get cached account
            var accountQuery = context.Accounts.Where(a => a.Api == api);
            Account account = accountQuery.SingleOrDefault();
            if (account == null) return userEntries;

            // Get all watch and archive entries (for query performance)
            IEnumerable<WatchEntry> watchEntries = await accountQuery.SelectMany(a => a.WatchEntries).IncludeDetails().ToListAsync();
            IEnumerable<ArchiveEntry> archiveEntries = await accountQuery.SelectMany(a => a.ArchiveEntries).IncludeDetails().ToListAsync();

            // Get all anime entries (for query performance)
            IEnumerable<Anime> animeEntries = await context.Animes.Where(a => a.Api == api)
                .OrderBy(a => a.Title)
                .IncludeDetails()
                .ToListAsync();

            // Create user entry for each anime entry
            foreach (Anime anime in animeEntries)
            {
                // Match watch entry
                WatchEntry watchEntry = watchEntries.Where(w => w.Anime.Id == anime.Id).SingleOrDefault();

                // Match archive entry
                ArchiveEntry archiveEntry = archiveEntries.Where(a => a.Anime.Id == anime.Id).SingleOrDefault();

                // Create a user entry if either (or both) watch entry or archive entry is present
                if (watchEntry != null || archiveEntry != null)
                {
                    userEntries.Add(anime);
                }
            }

            return userEntries;
        }

        public async Task<Anime> ResyncAnime(Anime anime)
        {
            try
            {
                Anime freshAnime = await apiService.GetApiClient(anime.Api).GetAnime(anime.Title);
                await SaveAnime(freshAnime);
            }
            catch (SearchNoResultException e)
            {
                Console.WriteLine(anime.Title + " failed resyncing because API didn't return anything");
                throw new Exception("For API polishing");
            }

            return await GetAnime(anime.ServerId, anime.Api);
        }

        public async Task ReloadAnime(Anime anime)
        {
            await Revert(anime);
        }

        public async Task<Anime> GetAnime(string animeId, string api)
        {
            var query = from anime in context.Animes
                        where anime.ServerId == animeId
                        where anime.Api == api
                        select anime;
            return await query.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Anime>> SearchAnime(string query, string api = null)
        {
            List<Anime> result = null;
            if (api == null) result = await context.Animes.ToListAsync();
            else await context.Animes.Where(a => a.Api == api).OrderBy(a => a.Title).ToListAsync();

            return result.Where(a => a.Match(query)).OrderBy(a => a.Title).ToList();
        }

        public async Task SaveAnime(Anime anime)
        {
            Console.WriteLine("Updating " + anime + "...");
            anime.DateUpdated = DateTime.Now;

            // Manually save object graph if it's detached
            if (context.Entry(anime).State == EntityState.Detached)
            {
                Anime original = await context.Animes.FirstOrDefaultAsync(a => a.ServerId == anime.ServerId && a.Api == anime.Api);

                // Update anime in the database based on freshly fetched anime
                if (original != null)
                {
                    original.Merge(anime);
                    context.Entry(original).State = EntityState.Modified;

                    // Synonym
                    if (anime.Synonyms.Count > 0)
                    {
                        var deletedSynonyms = original.Synonyms.Except(anime.Synonyms, s => s.Name);
                        var addedSynonyms = anime.Synonyms.Except(original.Synonyms, s => s.Name);
                        // Remove deleted synonyms
                        deletedSynonyms.ForEach(s => original.Synonyms.Remove(s));
                        foreach (Synonym s in deletedSynonyms)
                        {
                            original.Synonyms.Remove(s);
                            context.Synonyms.Remove(s);
                        }
                        // Add new synonyms
                        foreach (Synonym s in addedSynonyms)
                        {
                            if (context.Entry(s).State == EntityState.Detached && s.Id != 0) context.Synonyms.Attach(s);
                            original.Synonyms.Add(s);
                        }
                    }

                    // Genre
                    if (anime.Genres.Count > 0)
                    {
                        var deletedGenres = original.Genres.Except(anime.Genres, g => g.Name);
                        var addedGenres = anime.Genres.Except(original.Genres, g => g.Name);
                        // Remove deleted genres
                        deletedGenres.ForEach(g => original.Genres.Remove(g));
                        // Add new genres
                        foreach (Genre g in addedGenres)
                        {
                            if (context.Entry(g).State == EntityState.Detached && g.Id != 0) context.Genres.Attach(g);
                            original.Genres.Add(g);
                        }
                    }
                }

                // Add new anime to database
                else
                {
                    // Genre
                    var genreList = anime.Genres.ToList();
                    anime.Genres.Clear();
                    foreach (Genre genre in genreList)
                    {
                        var existingGenre = context.Genres.Where(g => g.Name == genre.Name).SingleOrDefault();
                        if (existingGenre != null) anime.Genres.Add(existingGenre);
                        else anime.Genres.Add(genre);
                    }

                    context.Animes.Add(anime);
                }
            }

            else
            {
                // Anime is attached to this context. Set it to Modified
                context.Entry(anime).State = EntityState.Modified;
            }

            await context.SaveChangesAsync();
        }

        public Anime MatchAnime(Anime other, string api)
        {
            using (var db = new TwinTailDb())
            {
                if (!other.ReferenceMatchable()) return null;
                if (other.Api == api) throw new Exception("Finding a match within the same API is not allowed");
                db.Configuration.LazyLoadingEnabled = false;

                List<Anime> all = db.Animes.Select(a => a).ToList();

                // Look for anime that has the same type, season, year
                var q1 = from anime in all
                         where anime.Api == api
                         where other.ReferenceMatchable()
                         where anime.Type == other.Type
                         where anime.Year == other.Year
                         where anime.Season == other.Season
                         select anime;

                // Match titles and synonyms
                var q2 = from anime in q1
                         where anime.Match(other.Title)
                         select anime;

                IList<Anime> result = q2.ToList();

                // Return immediately if result is 0 or 1 for performance
                if (result.Count == 0) return null;
                if (result.Count == 1) return result.First();

                // Get the anime with highest title relevance
                Anime closest = null;
                double highestRelevance = 0;
                foreach (Anime anime in result)
                {
                    if (closest == null)
                    {
                        closest = anime;
                        highestRelevance = StringHelper.LevenshteinPercentage(other.Title, anime.Title);
                        continue;
                    }

                    double relevance = StringHelper.LevenshteinPercentage(other.Title, anime.Title);
                    if (relevance > highestRelevance)
                    {
                        highestRelevance = relevance;
                        closest = anime;
                    }
                }

                return closest;
            }
        }
    }
}
