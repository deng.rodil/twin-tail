﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;
using Newtonsoft.Json.Linq;

namespace TwinTail.Services.Temp
{
    public class ArchiveTsvParser
    {
        public static string Parse()
        {
            string filePath = "C:\\Users\\sylph\\Desktop\\ArchiveExport.txt";
            FileInfo file = new FileInfo(filePath);
            if (!file.Exists) throw new Exception(file.FullName + " does not exist");

            string tsv;
            using (var stream = file.OpenText())
            {
                tsv = stream.ReadToEnd();
            }

            return Parse(tsv);
        }

        public static string Parse(string tsv)
        {
            JObject root = new JObject();
            JObject account = new JObject();
            root["account"] = account;
            account["user"] = "Sylpheed";
            account["api"] = "MyAnimeList";

            JArray entries = new JArray();
            root["entries"] = entries;

            string[] rows = tsv.Split('\n');
            foreach (string row in rows)
            {
                // Skip column header
                if (row == rows[0]) continue;
                // Skip empty rows
                if (string.IsNullOrEmpty(row)) continue;
                string[] columns = row.Split('\t');

                JObject entry = new JObject();
                Console.WriteLine("Parsing " + columns[1]);

                // Anime
                JObject anime = new JObject();
                entry["anime"] = anime;
                anime["id"] = columns[0];
                anime["api"] = "MyAnimeList";
                anime["title"] = columns[1];

                // Archive
                JObject archive = new JObject();
                entry["archive"] = archive;

                try
                {
                    if (columns[2] == "dropped") continue;
                    archive["status"] = ConvertStatus(columns[2]).ToString();
                }
                catch
                {
                    archive["status"] = ArchiveStatus.Missing.ToString();
                }
                
                try
                {
                    archive["source"] = ConvertSource(columns[3]).ToString();
                }
                catch
                {
                    archive["source"] = VideoSource.TV.ToString();
                }

                try
                {
                    archive["notes"] = columns[7];
                }
                catch
                {
                    archive["notes"] = null;
                }

                try
                {
                    archive["video_width"] = Int32.Parse(columns[4]);
                }
                catch
                {
                    archive["video_width"] = 0;
                }

                try
                {
                    archive["video_height"] = Int32.Parse(columns[5]);
                }
                catch
                {
                    archive["video_height"] = 0;
                }

                // Fansub
                JArray fansubs = new JArray();
                entry["fansubs"] = fansubs;
                try
                {
                    string[] fansubArray = columns[6].Split(',');
                    foreach (string fansub in fansubArray)
                    {
                        if (string.IsNullOrEmpty(fansub)) continue;
                        if (string.IsNullOrWhiteSpace(fansub)) continue;

                        JObject jFansub = new JObject();
                        jFansub["name"] = fansub;
                        fansubs.Add(jFansub);
                    }
                }
                catch
                {

                }

                entries.Add(entry);
            }

            // Save file to desktop
            string json = root.ToString();

            string filePath = "C:\\Users\\sylph\\Desktop\\archive-Sylpheed.json";
            FileInfo file = new FileInfo(filePath);
            using (var stream = file.CreateText())
            {
                stream.Write(json);
            }

            return json;
        }

        private static ArchiveStatus ConvertStatus(string status)
        {
            switch (status)
            {
                case "ok":
                    return ArchiveStatus.Archived;
                case "downloading":
                    return ArchiveStatus.Downloading;
                case "pending":
                case "airing":
                    return ArchiveStatus.Pending;
                case "missing":
                    return ArchiveStatus.Missing;
                case "recompile":
                    return ArchiveStatus.Broken;
                default:
                    throw new Exception("Invalid status");
            }
        }

        private static VideoSource ConvertSource(string source)
        {
            switch (source)
            {
                case "bd":
                    return VideoSource.BD;
                case "tv":
                    return VideoSource.TV;
                case "dvd":
                    return VideoSource.DVD;
                case "":
                case null:
                    return VideoSource.TV;
                default:
                    throw new Exception("Invalid source");
            }
        }
    }
}
