﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace TwinTail.Services
{
    public class GenericRepository<TEntity, TContext>
        where TEntity : class
        where TContext : DbContext
    {

        protected TContext context;
        protected DbSet<TEntity> dbSet;

        public GenericRepository(TContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            // Where filter
            if (filter != null) query = query.Where(filter);

            // Include property
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            // Sort
            if (orderBy != null) return orderBy(query).ToList();

            return query.ToList();
        }

        public virtual TEntity GetByPk(object primaryKey)
        {
            return dbSet.Find(primaryKey);
        }

        public virtual void Add(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object primaryKey)
        {
            TEntity entityToDelete = dbSet.Find(primaryKey);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached) dbSet.Attach(entity);

            dbSet.Remove(entity);
        }

        public virtual void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void AddOrUpdate(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached) Update(entity);
            else Add(entity);

        }

        public virtual async Task Revert(TEntity entity)
        {
            await context.Entry(entity).ReloadAsync();
        }
    }
}
