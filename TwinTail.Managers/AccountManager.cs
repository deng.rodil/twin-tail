﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoLib;
using Sylpheed.Helpers;
using TwinTail.Entities;

namespace TwinTail.Services
{
    public class AccountManager : GenericRepository<Account, TwinTailDb>, IAccountService
    {
        private static string SALT_PASSWORD = "+WiN+@1l";

        private IApiService apiService;
        private Crypto crypto = new Crypto();
        private List<string> authenticationList = new List<string>();

        public IEnumerable<Account> Accounts
        {
            get
            {
                return context.Accounts.ToList();
            }
        }

        public AccountManager(TwinTailDb context, IApiService apiService)
            : base(context)
        {
            this.apiService = apiService;
        }

        public async Task LoadAccounts()
        {
            var query = from account in context.Accounts
                        where account.Username != null
                        where account.Password != null
                        select account;

            foreach (Account account in query)
            {
                string decryptedPass = crypto.Decrypt(account.Password, SALT_PASSWORD);
                await apiService.GetApiClient(account.Api).Authenticate(account.Username, decryptedPass);
            }
        }

        public async Task<bool> Authenticate(string api)
        {
            try
            {
                // Check if account exists
                Account acc = context.Accounts.SingleOrDefault(a => a.Api == api);
                if (acc == null) return false;

                // Authenticate account
                await apiService.GetApiClient(api).Authenticate(acc.Username, crypto.Decrypt(acc.Password, SALT_PASSWORD));
                authenticationList.Add(api);
                return true;
            }
            catch
            {
                authenticationList.Remove(api);
                return false;
            }
        }

        public async Task<bool> SetupAccount(string api, string username, string password)
        {
            try
            {
                // Authenticate account
                await apiService.GetApiClient(api).Authenticate(username, password);

                // Check if account of the same API exists. Only one account per API may exist.
                Account acc = context.Accounts.SingleOrDefault(a => a.Api == api);

                // Add account if it doesn't exist yet
                if (acc == null)
                {
                    acc = new Account();
                    acc.Username = username;
                    acc.Password = crypto.Encrypt(password, SALT_PASSWORD);
                    acc.Api = api;
                    context.Accounts.Add(acc);
                }

                else
                {
                    // Remove account if it has a different username
                    if (acc.Username != username)
                    {
                        context.Accounts.Remove(acc);

                        acc = new Account();
                        acc.Username = username;
                        acc.Password = crypto.Encrypt(password, SALT_PASSWORD);
                        acc.Api = api;
                        context.Accounts.Add(acc);
                    }

                    // Update password (in case user changed password)
                    else
                    {
                        acc.Password = crypto.Encrypt(password, SALT_PASSWORD);
                    }
                }

                // Save changes and mark as authenticated
                await context.SaveChangesAsync();
                authenticationList.Add(api);
                return true;
            }
            catch
            {
                authenticationList.Remove(api);
                return false;
            }
        }

        public bool IsAuthenticated(string api)
        {
            return authenticationList.Contains(api);
        }

        public Account GetAccount(string api)
        {
            return context.Accounts.SingleOrDefault(a => a.Api == api);
        }
    }
}
