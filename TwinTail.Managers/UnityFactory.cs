﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.Unity;

namespace TwinTail.Services
{
    public class UnityFactory : IFactory
    {
        private IUnityContainer _container;

        public UnityFactory(IUnityContainer container)
        {
            _container = container;
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public T BuildUp<T>(T obj)
        {
            return _container.BuildUp<T>(obj);
        }
    }
}
