﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TwinTail.Entities;
using TwinTail.Api;

namespace TwinTail.Services
{
    public class ApiManager : IApiService
    {
        private Dictionary<string, ApiClient> apiClients = new Dictionary<string, ApiClient>();
        public IEnumerable<ApiClient> Clients { get { return apiClients.Values; } }

        public ApiClient DefaultClient { get; set; }

        public ApiManager()
        {
            RegisterApi(new MalApiClient(), true);
        }

        public ApiClient GetApiClient(string api)
        {
            try
            {
                return apiClients.Values.Single(a => a.Id == api);
            }
            catch
            {
                throw new ApiClientNotRegisteredException(api);
            }
        }

        public void RegisterApi(ApiClient api, bool setDefault = false)
        {
            apiClients[api.Id] = api;
            if (setDefault) DefaultClient = api;
        }

        public void ResignApi(string api)
        {
            ApiClient client = GetApiClient(api);
            apiClients.Remove(api);
        }
    }

    
}
