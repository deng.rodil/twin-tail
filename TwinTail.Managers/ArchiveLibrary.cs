﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using TwinTail.Entities;
using Sylpheed.Helpers;
using Newtonsoft.Json.Linq;
using System.IO;

namespace TwinTail.Services
{
    public class ArchiveLibrary : GenericRepository<ArchiveEntry, TwinTailDb>, IArchiveService
    {
        public ArchiveLibrary(TwinTailDb context)
            : base(context)
        {
        }

        public async Task<IEnumerable<ArchiveEntry>> GetLibrary(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            // Get archive entries
            var query = context.ArchiveEntries.Where(a => a.Account.Id == account.Id);
            List<ArchiveEntry> entries = await query.OrderBy(a => a.Anime.Title).ToListAsync();

            return entries;
        }

        public async Task<ArchiveEntry> GetEntry(Account account, Anime anime)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            // Get archived entry
            return await context.ArchiveEntries.Where(a => a.Anime.Id == anime.Id && a.Account.Id == account.Id).SingleOrDefaultAsync();
        }

        public async Task<ArchiveEntry> SearchEntry(Account account, string searchKey)
        {
            throw new NotImplementedException();
        }

        public async Task SaveEntry(ArchiveEntry entry)
        {
            // Manually update object graph if entity is detached
            if (context.Entry(entry).State == EntityState.Detached)
            {
                ArchiveEntry original = await context.ArchiveEntries.Where(a => a.Id == entry.Id).SingleOrDefaultAsync();
                original.DateUpdated = DateTime.Today;

                // Update
                if (original != null)
                {
                    // Fansub
                    var deletedFansubs = original.Fansubs.Except(entry.Fansubs, f => f.Id);
                    var addedFansubs = entry.Fansubs.Except(original.Fansubs, f => f.Id);
                    // Remove deleted fansubs
                    deletedFansubs.ForEach(f => original.Fansubs.Remove(f));
                    // Add new fansubs
                    foreach (Fansub f in addedFansubs)
                    {
                        if (context.Entry(f).State == EntityState.Detached && f.Id != 0) context.Fansubs.Attach(f);
                        original.Fansubs.Add(f);
                    }
                }

                // Add
                else
                {
                    context.ArchiveEntries.Add(entry);

                    var fansubList = entry.Fansubs.ToList();
                    entry.Fansubs.Clear();
                    foreach (Fansub fansub in fansubList)
                    {
                        var existingFansub = context.Fansubs.Where(f => f.Id == fansub.Id).SingleOrDefault();
                        if (existingFansub != null) entry.Fansubs.Add(existingFansub);
                        else entry.Fansubs.Add(fansub);
                    }
                }
            }

            else
            {
                // Set to modified if entity is already attached
                entry.DateUpdated = DateTime.Today;
                context.Entry(entry).State = EntityState.Modified;
            }

            await context.SaveChangesAsync();
        }

        public async Task ReloadEntry(ArchiveEntry entry)
        {
            await Revert(entry);
        }

        public async Task DeleteEntry(ArchiveEntry entry)
        {
            Delete(entry);
            await context.SaveChangesAsync();
        }

        public async Task<string> ExportEntries(Account account)
        {
            // Get all entries for API
            IEnumerable<ArchiveEntry> entries = await GetLibrary(account);
            if (!entries.Any()) return null;

            // Serialize to json
            JObject root = new JObject();

            // Account
            JObject jAccount = new JObject();
            root["account"] = jAccount;
            Account acc = entries.First().Account;
            jAccount["user"] = acc.Username;
            jAccount["api"] = acc.Api;

            // Archive Entries
            JArray jEntries = new JArray();
            root["entries"] = jEntries;
            foreach (ArchiveEntry entry in entries)
            {
                JObject jEntry = new JObject();
                jEntries.Add(jEntry);

                // Anime
                JObject anime = new JObject();
                jEntry["anime"] = anime;
                anime["api"] = account.Api;
                anime["id"] = entry.Anime.ServerId;
                anime["title"] = entry.Anime.Title;
                anime["image_url"] = entry.Anime.ImageUrl;
                anime["date_started"] = entry.Anime.DateStarted;
                anime["date_ended"] = entry.Anime.DateEnded;

                // Archive
                JObject archive = new JObject();
                jEntry["archive"] = archive;
                archive["status"] = entry.Status.ToString();
                archive["source"] = entry.Source.ToString();
                archive["video_width"] = entry.VideoWidth;
                archive["video_height"] = entry.VideoHeight;
                archive["notes"] = entry.Notes;
                archive["date_updated"] = entry.DateUpdated;

                // Fansub
                JArray jFansubs = new JArray();
                jEntry["fansubs"] = jFansubs;
                foreach (Fansub fansub in entry.Fansubs)
                {
                    JObject jFansub = new JObject();
                    jFansubs.Add(jFansub);

                    jFansub["name"] = fansub.ShortName;
                }
            }

            // Convert to json string
            string json = root.ToString();

            // Save to local disk (TODO: Save to user-defined path)
            string cacheDirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\TwinTail\\archive\\";
            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + account.Api + "\\");
            if (!dir.Exists) dir.Create();

            FileInfo file = new FileInfo(dir.FullName + "archive-" + entries.First().Account.Username + ".json");
            if (file.Exists) file.Delete();
            using (var writer = file.CreateText())
            {
                await writer.WriteAsync(json);
            }

            return json;
        }

        public async Task ImportEntries(string json)
        {
            JObject root = JObject.Parse(json);
            string user = (string)root["account"]["user"];
            string api = (string)root["account"]["api"];

            // Get cached account
            Account account = context.Accounts.Where(a => a.Api == api && a.Username == user).SingleOrDefault();
            if (account == null) throw new Exception("No account is logged in for " + api);

            JArray jEntries = root["entries"] as JArray;

            // Get all entries for API
            IEnumerable<ArchiveEntry> entries = await GetLibrary(account);

            // Parse entries
            foreach (JObject jEntry in jEntries)
            {
                JObject jAnime = jEntry["anime"] as JObject;
                JObject jArchive = jEntry["archive"] as JObject;
                JArray jFansubs = jEntry["fansubs"] as JArray;

                Console.WriteLine("Importing " + jAnime["title"] + " (" + jAnime["id"] + ")...");
                string animeId = (string)jAnime["id"];

                // Look if archive already exists
                ArchiveEntry entry = entries.Where(a => a.Anime.Api == api && a.Anime.ServerId == animeId).SingleOrDefault();

                // Create new entry if it doesn't exist yet
                if (entry == null)
                {
                    entry = new ArchiveEntry();
                    entry.Account = account;
                    context.ArchiveEntries.Add(entry);

                    // Add anime entry if it doesn't exist in the database yet
                    Anime anime = await context.Animes.Where(a => a.ServerId == animeId && a.Api == api).SingleOrDefaultAsync();
                    if (anime == null)
                    {
                        anime = new Anime();
                        anime.ServerId = animeId;
                        anime.Api = api;
                        anime.Title = (string)jAnime["title"];

                        DateTime dateStarted;
                        if (DateTime.TryParse((string)jAnime["date_started"], out dateStarted))
                        {
                            anime.DateStarted = dateStarted;
                        }

                        DateTime dateEnded;
                        if (DateTime.TryParse((string)jAnime["date_ended"], out dateEnded))
                        {
                            anime.DateEnded = dateEnded;
                        }
                    }

                    entry.Anime = anime;
                }

                // Update archive entry
                entry.Status = (ArchiveStatus)Enum.Parse(typeof(ArchiveStatus), (string)jArchive["status"]);
                entry.Source = (VideoSource)Enum.Parse(typeof(VideoSource), (string)jArchive["source"]);
                entry.VideoWidth = (int)jArchive["video_width"];
                entry.VideoHeight = (int)jArchive["video_height"];
                entry.Notes = (string)jArchive["notes"];
                DateTime dateUpdated;
                if (DateTime.TryParse((string)jArchive["date_updated"], out dateUpdated)) entry.DateUpdated = dateUpdated;

                // Fansubs
                entry.Fansubs.Clear();
                foreach (JObject jFansub in jFansubs)
                {
                    string name = (string)jFansub["name"];

                    // Check if fansub already exists
                    Fansub fansub = context.Fansubs.Where(f => f.ShortName == name).SingleOrDefault();
                    if (fansub == null)
                    {
                        fansub = new Fansub();
                        fansub.ShortName = name;

                        // Commit changes to register the new fansub entry
                        context.Fansubs.Add(fansub);
                        await context.SaveChangesAsync();
                    }

                    entry.Fansubs.Add(fansub);
                }
            }

            // Commit changes
            await context.SaveChangesAsync();
        }
    }
}
