﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace TwinTail.Services
{
    public class UnitOfWork<TContext> : IDisposable
        where TContext : DbContext, new()
    {
        private bool saveCalled = false;

        public TContext Context { get; private set; }
        public bool SaveOnDispose { get; private set; }
        public bool RevertOnSaveNotCalled { get; private set; }

        public UnitOfWork(bool saveOnDispose = false, bool revertOnSaveNotCalled = false)
        {
            Context = new TContext();
            SaveOnDispose = saveOnDispose;
            RevertOnSaveNotCalled = revertOnSaveNotCalled;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
            saveCalled = true;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
            saveCalled = true;
        }

        public void RevertChanges()
        {
            foreach (DbEntityEntry entry in Context.ChangeTracker.Entries())
            {
                entry.Reload();
            }
        }

        public async Task RevertChangesAsync()
        {
            foreach (DbEntityEntry entry in Context.ChangeTracker.Entries())
            {
                await entry.ReloadAsync();
            }
        }

        #region IDisposable
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (SaveOnDispose) SaveChanges();
                    if (!saveCalled && RevertOnSaveNotCalled) RevertChanges();

                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion // IDisposable
    }
}
