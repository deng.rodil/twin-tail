﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using TwinTail.Entities;

namespace TwinTail.Managers
{
    public class AnimeReferenceManager
    {
        public ApiManager ApiManager { get; set; }
        public AnimeDatabase AnimeDb { get; set; }

        public Reference GetReference(int animeId)
        {
            Reference result;
            using (var db = new TwinTailDb())
            {
                var query = from reference in db.References
                            where reference.Animes.Any(a => a.Id == animeId)
                            select reference;
                result = query.SingleOrDefault();
            }

            return result;
        }

        public Reference GetReference(string serverId, AnimeApi api)
        {
            Reference result;
            using (var db = new TwinTailDb())
            {
                var query = from reference in db.References
                            where reference.Animes.Any(a => a.Api == api && a.ServerId == serverId)
                            select reference;
                result = query.SingleOrDefault();
            }

            return result;
        }

        public async Task UpdateReference(Anime anime)
        {
            if (anime.Id == 0) throw new Exception("Cannot update reference for entries that aren't fetched from local database");

            Reference reference = GetReference(anime.Id);

            using (var db = new TwinTailDb())
            {
                if (reference == null)
                {
                    reference = new Reference();
                    reference.IncludeAnime(anime);
                    db.References.Add(reference);
                }
                else
                {
                    db.References.Attach(reference);
                }

                HashSet<AnimeApi> missingReferences = new HashSet<AnimeApi>(ApiManager.EnabledApiServices);
                missingReferences.IntersectWith(reference.MissingReferences);
                foreach (AnimeApi api in missingReferences)
                {
                    // Search for local database
                    Anime match = AnimeDb.MatchAnime(anime, api);

                    if (match != null)
                    {
                        Debug.WriteLine("Local Reference: {0} == {1}", anime, match);
                        reference.IncludeAnime(match);
                        continue;
                    }

                    // Search for server
                    match = await ApiManager.GetApiClient(api).Match(anime);
                    if (match != null)
                    {
                        match = AnimeDb.GetAnime(match.ServerId, api);

                        reference.IncludeAnime(match);
                        Debug.WriteLine("Server Reference: {0} == {1}", anime, match);
                        continue;
                    }

                    Debug.WriteLine("No reference found for {0}", anime);
                }
            }
        }

        //private async Task<Anime> MatchWithLocal(Anime other)
        //{

        //}

        //private async Task<Anime> MatchWithServer(Anime other)
        //{

        //}
    }
}