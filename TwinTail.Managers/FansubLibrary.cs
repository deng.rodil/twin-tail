﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public class FansubLibrary : GenericRepository<Fansub, TwinTailDb>, IFansubService
    {
        public FansubLibrary(TwinTailDb context)
            : base(context)
        {

        }

        public async Task<IEnumerable<Fansub>> GetAll()
        {
            return await context.Fansubs.OrderBy(f => f.ShortName).ToListAsync();
        }

        public async Task<Fansub> Search(Expression<Func<Fansub, bool>> predicate)
        {
            return await context.Fansubs.Where(predicate).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Fansub>> SearchMany(Expression<Func<Fansub, bool>> predicate)
        {
            return await context.Fansubs.Where(predicate).OrderBy(f => f.ShortName).ToListAsync();
        }
    }
}
