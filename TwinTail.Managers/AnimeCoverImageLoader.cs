﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using TwinTail.Entities;

namespace TwinTail.Services
{
    public class AnimeCoverImageLoader : IAnimeCoverImageLoader
    {
        private string cacheDirPath;

        public AnimeCoverImageLoader()
        {
            // Setup file path
            cacheDirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\TwinTail\\cover\\";
        }

        public BitmapImage LoadLocalImage(Anime anime)
        {
            if (anime == null) throw new ArgumentNullException(nameof(anime));
            if (anime.Api == null) throw new ArgumentNullException("Anime.Api cannot be null");

            string path = GetImagePath(anime);
            if (path == null) return null;

            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.UriSource = new Uri(path);
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.EndInit();
            return img;
        }

        public async Task<BitmapImage> LoadRemoteImage(Anime anime)
        {
            if (anime == null) throw new ArgumentNullException(nameof(anime));
            if (anime.Api == null) throw new ArgumentNullException("Anime.Api cannot be null");
            if (anime.ImageUrl == null) throw new ArgumentNullException("Anime.ImageUrl cannot be null");

            // Setup IO
            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + anime.Api + "\\");
            if (!dir.Exists) dir.Create();

            FileInfo file = new FileInfo(dir.FullName + anime.ServerId + ".jpg");

            // Download remote image
            bool downloaded = await DownloadRemoteImageFile(anime.ImageUrl, file);
            if (!downloaded) return null;

            // Load bitmap from cache
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.UriSource = new Uri(file.FullName);
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.EndInit();
            return img;
        }

        public async Task<BitmapImage> LoadImage(Anime anime)
        {
            BitmapImage img = LoadLocalImage(anime);
            if (img == null) img = await LoadRemoteImage(anime);

            return img;
        }

        private async Task DownloadRemoteImage(Anime anime)
        {
            if (anime == null) throw new ArgumentNullException(nameof(anime));
            if (anime.Api == null) throw new ArgumentNullException("Anime.Api cannot be null");
            if (anime.ImageUrl == null) throw new ArgumentNullException("Anime.ImageUrl cannot be null");

            // Setup IO
            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + anime.Api + "\\");
            if (!dir.Exists) dir.Create();

            FileInfo file = new FileInfo(dir.FullName + anime.ServerId + ".jpg");

            await DownloadRemoteImageFile(anime.ImageUrl, file);
        }

        public string GetImagePath(Anime anime)
        {
            DirectoryInfo dir = new DirectoryInfo(cacheDirPath + "\\" + anime.Api + "\\");
            if (!dir.Exists) return null;

            FileInfo file = new FileInfo(dir.FullName + anime.ServerId + ".jpg");
            if (!file.Exists) return null;

            return file.FullName;
        }

        private async Task<bool> DownloadRemoteImageFile(string uri, FileInfo saveFile)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response;
            try
            {
                response = await request.GetResponseAsync() as HttpWebResponse;
            }
            catch (Exception)
            {
                return false;
            }

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                // if the remote file was found, download it
                using (FileStream outputStream = saveFile.OpenWrite())
                using (Stream inputStream = response.GetResponseStream())
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = await inputStream.ReadAsync(buffer, 0, buffer.Length);
                        await outputStream.WriteAsync(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }

                return true;
            }

            return false;
        }
    }
}
